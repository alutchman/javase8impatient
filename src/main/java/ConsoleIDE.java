import java.io.Console;
import java.io.IOException;

public class ConsoleIDE {
    public static void main(String[] args){
        Console console = System.console();
        if (console == null) {
            System.err.println("Cannot retrieve colsole object");
            System.exit(-1);
        }
        console.printf("Type something....");
        console.printf(console.readLine());
    }
}

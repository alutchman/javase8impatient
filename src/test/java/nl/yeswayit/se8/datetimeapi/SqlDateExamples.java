package nl.yeswayit.se8.datetimeapi;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.sql.Date;

public class SqlDateExamples {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void showExamples(){
        Date sqlDate = new Date(new java.util.Date().getTime());
        System.out.println("sqlDate = " +
                new java.sql.Date(new java.util.Date().getTime()));
        System.out.println("localDate = " + sqlDate.toLocalDate());
        System.out.println("localDate = " + sqlDate.toLocalDate());
    }

    @Test
    public void unsupportedOperationException(){
        expectedException.expect(UnsupportedOperationException.class);
        Date sqlDate = new Date(new java.util.Date().getTime());
        sqlDate.toInstant();
    }
}

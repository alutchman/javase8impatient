package nl.yeswayit.se8.datetimeapi;

import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.ZoneId;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class YearDataOptions {

    @Test
    public void showYearExamples(){
        Year currentYear = Year.now();
        Year twoThousand = Year.of(2000);
        boolean isLeap = currentYear.isLeap(); // false
        int length = currentYear.length(); // 365
        LocalDate date = Year.of(2014).atDay(64);
        System.out.format("huidigJaar = %d\nJaar 2000 = %d \n",currentYear.getValue(), twoThousand.getValue()) ;
        Year.now(ZoneId.of("Europe/Amsterdam"));
        LocalDate asDateOnly = Year.of(2014).atMonth(11).atDay(30);
        System.out.println(asDateOnly);
        LocalDateTime asDateAndTime = Year.of(2014).atMonth(11).
                atDay(30).atTime(12, 30, 15);
        System.out.println(asDateAndTime);
    }

}

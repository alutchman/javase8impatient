package nl.yeswayit.se8.datetimeapi;

import org.junit.Test;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.Period;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LearnLocalDateTime {

    @Test
    public void basicTest(){
        LocalDateTime now  = LocalDateTime.now(Clock.systemDefaultZone());
        System.out.println("now.plus(3, ChronoUnit.HOURS)) :"+  now.plus(3, ChronoUnit.HOURS));
        System.out.println("now.minusHours(44) :"+  now.minusHours(44));
        System.out.println("now.plusDays(2) :"+  now.plusDays(2));
    }

    @Test
    public void testInstant(){
        Instant now = Instant.now();
        System.out.println("now :"+  now);
        System.out.println("now + 30 days :"+  now.plus(30, ChronoUnit.DAYS));
        System.out.println("Instant.from(ZonedDateTime.now()) :"+  Instant.from(ZonedDateTime.now()));

        OffsetDateTime offset
                = OffsetDateTime.now();

        // print Value
        System.out.println("OffsetDateTime: "
                + offset);

        // apply from() method
        Instant result = Instant.from(offset);

        // print result
        System.out.println("Instant: "
                + result);
    }

    @Test
    public void testEpochEtc(){
        Instant now = Instant.now();
        System.out.println("now.getEpochSecond() = "+ now.getEpochSecond());
        System.out.println("now.toEpochMilli() = "+ now.toEpochMilli());
    }

    @Test
    public void testPeriodDuration(){
        LocalDateTime dateStart = LocalDateTime.of(2019,1, 1, 12,0,0);
        LocalDateTime dateEnd = LocalDateTime.of(2020,3, 5, 15,30,0);
        Period fromLocalDateTimes = Period.between(dateStart.toLocalDate(), dateEnd.toLocalDate());
        System.out.println(fromLocalDateTimes);
        assertEquals(2, fromLocalDateTimes.getMonths());
        assertEquals(4, fromLocalDateTimes.getDays());
        assertEquals(1, fromLocalDateTimes.getYears());

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime later =  LocalDateTime.now().plusDays(1).plusHours(3).plusMinutes(15).plusSeconds(30);
        Duration duration = Duration.between(now, later);
        System.out.println(duration);
        assertTrue(duration.toString().startsWith("PT27H15M30"));
        assertEquals(15+27*60, ChronoUnit.MINUTES.between(now, later));
        assertEquals(27, ChronoUnit.HOURS.between(now, later));
        assertEquals(1, ChronoUnit.DAYS.between(now, later));
    }

    @Test
    public void showParts(){
        LocalDateTime localDateTime = LocalDateTime.of(2016,04, 27,
                12, 30, 15);
        assertEquals(2016, localDateTime.getYear());
        assertEquals(4, localDateTime.getMonthValue());
        assertEquals(Month.APRIL, localDateTime.getMonth());
        assertEquals(27, localDateTime.getDayOfMonth());
        assertEquals(12, localDateTime.getHour());
        assertEquals(30, localDateTime.getMinute());
        assertEquals(15, localDateTime.getSecond());
    }
}

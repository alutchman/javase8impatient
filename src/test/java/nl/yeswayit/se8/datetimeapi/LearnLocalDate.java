package nl.yeswayit.se8.datetimeapi;

import lombok.extern.java.Log;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.UnsupportedTemporalTypeException;

@Log
public class LearnLocalDate {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testLocalDate() {
        LocalDate today = LocalDate.now();
        System.out.println("Vandaag ="+ today);
        System.out.println("today.getDayOfWeek() === Dag van de week ="+ today.getDayOfWeek());

        LocalDate sint = LocalDate.of(2018,12, 5);
        System.out.println("Start datum ="+ sint);
        LocalDate expired = sint.plusDays(365*5);
        System.out.println("Expired datum(5 jaren) ="+ expired);

        System.out.println("expired.plusDays(2) ="+ expired.plusDays(2));
        System.out.println("expired.plusMonths(2) ="+ expired.plusMonths(2));
        System.out.println("expired.plusWeeks(2) ="+ expired.plusWeeks(2));
        System.out.println("expired.plusYears(2) ="+ expired.plusYears(2));
        System.out.println("expired.minusDays(30) ="+ expired.minusDays(30));

        LocalDate asia = LocalDate.now(ZoneId.of("Asia/Kolkata"));
        System.out.println("asia  ="+ asia);

        LocalDate usa = LocalDate.now(ZoneId.of("US/Eastern"));
        System.out.println("usa Eastern = "+ usa);
    }

    @Test
    public void testLocalDateOfYearDay(){
        LocalDate Day60Of2020 = LocalDate.ofYearDay(2020, 60);
        System.out.println(" LocalDate.ofYearDay(2020, 60) = "+ Day60Of2020);
    }

    @Test
    public void testParse(){
        LocalDate mother = LocalDate.parse("1941-03-22");
        System.out.println(" LocalDate.parse(\"1941-03-22\") = "+ mother);
    }

    @Test
    public void testDayOfEpoch() {
        LocalDate jaarplus40  = LocalDate.ofEpochDay(365*40);
        System.out.println(" LocalDate.ofEpochDay(365*40) = "+ jaarplus40);
    }

    @Test
    public void testDedaultZoneId() {
        LocalDate date  = LocalDate.now(Clock.systemDefaultZone());
        System.out.println("LocalDate systemDefaultZone  = LocalDate.now(Clock.systemDefaultZone()) = "+ date);
    }

    @Test
    public void testPlus() {
        LocalDate date  = LocalDate.now(Clock.systemDefaultZone());
        System.out.println("LocalDate systemDefaultZone  = LocalDate.now(Clock.systemDefaultZone()) = "+ date);
        System.out.println("date.plus(10, ChronoUnit.MINUTES)  = "+date.plus(10, ChronoUnit.DAYS));
    }

    @Test
    public void testPlusUnhappyFlow() {
        expectedException.expect(UnsupportedTemporalTypeException.class);
        expectedException.expectMessage("Unsupported unit: Minutes");
        LocalDate date  = LocalDate.now(Clock.systemDefaultZone());
        System.out.println("LocalDate systemDefaultZone  = LocalDate.now(Clock.systemDefaultZone()) = "+ date);
        System.out.println("date.plus(10, ChronoUnit.MINUTES)  = "+date.plus(10, ChronoUnit.MINUTES));
    }

    @Test
    public void getParts(){
        LocalDate localDate  = LocalDate.now(Clock.systemDefaultZone());
        System.out.println("localDate.getLong(ChronoField.DAY_OF_MONTH): "+
                localDate.getLong(ChronoField.DAY_OF_MONTH));
        System.out.println("localDate.getLong(ChronoField.MONTH_OF_YEAR): "+
                localDate.getLong(ChronoField.MONTH_OF_YEAR));
        System.out.println("localDate.getLong(ChronoField.YEAR): "+
                localDate.getLong(ChronoField.YEAR));
        System.out.println("localDate.getLong(ChronoField.DAY_OF_WEEK): "+
                localDate.getLong(ChronoField.DAY_OF_WEEK));
        System.out.println("localDate.getLong(ChronoField.DAY_OF_YEAR): "+
                localDate.getLong(ChronoField.DAY_OF_YEAR));
        System.out.println("localDate.getDayOfMonth(): "+
                localDate.getDayOfMonth());
        System.out.println("localDate.getYear(): "+
                localDate.getYear());
        System.out.println("localDate.getDayOfMonth(): "+
                localDate.getDayOfMonth());
    }


    @Test
    public void getPartsViolation(){
        expectedException.expect(UnsupportedTemporalTypeException.class);
        expectedException.expectMessage("Unsupported field: HourOfDay");
        LocalDate localDate  = LocalDate.now(Clock.systemDefaultZone());
        localDate.getLong(ChronoField.HOUR_OF_DAY);
    }

}

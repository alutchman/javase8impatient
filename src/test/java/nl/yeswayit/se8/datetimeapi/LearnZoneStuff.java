package nl.yeswayit.se8.datetimeapi;

import org.junit.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;

import static org.junit.Assert.assertEquals;

public class LearnZoneStuff {

    @Test
    public void testZoneOffset(){
        LocalDateTime nu = LocalDateTime.now();
        ZonedDateTime zonedDateTime = nu.atZone(ZoneId.of("Europe/Amsterdam"));
        ZoneOffset zoneOffset = zonedDateTime.getOffset();
        System.out.println(zoneOffset);
        assertEquals(7200, zoneOffset.get(ChronoField.OFFSET_SECONDS));
        assertEquals("+02:00", zoneOffset.getId());

    }

    @Test
    public void testTimeDifference(){
        ZoneId amsterdam = ZoneId.of("Europe/Amsterdam" );
        ZonedDateTime amsDateTime = ZonedDateTime.of(
                LocalDateTime.of(2020, Month.JANUARY, 8,6,0),amsterdam);

        ZoneId newYorkZone = ZoneId.of("US/Eastern");
        ZonedDateTime newYork = amsDateTime.withZoneSameInstant(newYorkZone);

        Duration timeDif = Duration.between(newYork.toLocalTime(), amsDateTime.toLocalTime());
        assertEquals(6, timeDif.toHours());
    }


    @Test
    public void printUsaZoneIds(){
        ZoneId.getAvailableZoneIds().stream().filter(x -> x.startsWith("US/")).sorted().forEach(System.out::println);
    }

    @Test
    public void printEUZoneIds(){
        ZoneId.getAvailableZoneIds().stream().filter(x -> x.startsWith("Europe/")).sorted().forEach(System.out::println);
    }

    @Test
    public void printAmericaZoneIdsALL(){
        ZoneId.getAvailableZoneIds().stream().filter(x -> x.startsWith("America/")).sorted().forEach(System.out::println);
    }

    @Test
    public void flightTravelExample(){
        LocalDateTime depart = LocalDateTime.of(2019,12,25, 10, 05,0);
        ZonedDateTime zonedDateTimeLeave = ZonedDateTime.of(depart, ZoneId.of("Europe/Amsterdam"));

        ZonedDateTime arrival = zonedDateTimeLeave.withZoneSameInstant(ZoneId.of("America/Curacao")).plusHours(10);

        System.out.println("left at "+ zonedDateTimeLeave);
        System.out.println("arrived at "+ arrival);

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd MMM YYYY hh:mm:ss a");
        System.out.println("left at "+ dateTimeFormatter.format(zonedDateTimeLeave));
        System.out.println("arrived at "+ dateTimeFormatter.format(arrival));
    }

    @Test
    public void dayLightSavingTime(){
        LocalDateTime winter = LocalDateTime.of(2020, 11,5,
                4,20,5);
        LocalDateTime summer = LocalDateTime.of(2020, 8,15,
                4,20,5);
        ZoneId zoneId1 = ZoneId.of("Europe/Amsterdam");
        ZoneId zoneId2 = ZoneId.of("America/Curacao");

        Duration europe = zoneId1.getRules().getDaylightSavings(winter.atZone(zoneId1).toInstant());
        Duration curacao = zoneId2.getRules().getDaylightSavings(winter.atZone(zoneId2).toInstant());
        System.out.printf("NL daylight saving winter: %d\n", europe.toHours() );
        System.out.printf("CUR daylight saving winter: %d\n", curacao.toHours() );


        europe = zoneId1.getRules().getDaylightSavings(summer.atZone(zoneId1).toInstant());
        curacao = zoneId2.getRules().getDaylightSavings(summer.atZone(zoneId2).toInstant());
        System.out.printf("NL daylight saving summer: %d\n", europe.toHours() );
        System.out.printf("CUR daylight saving summer: %d\n", curacao.toHours() );

    }
}

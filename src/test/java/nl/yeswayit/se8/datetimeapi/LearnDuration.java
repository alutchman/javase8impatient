package nl.yeswayit.se8.datetimeapi;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.DateTimeException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalField;
import java.time.temporal.UnsupportedTemporalTypeException;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LearnDuration {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void showChronos(){
        Arrays.stream(ChronoUnit.values()).
                map(ChronoUnit::toString).
                map(String::toUpperCase).forEach(System.out::println);
    }

    @Test
    public void testDuration2Weeks(){
      Duration week2 =    Duration.of(14, ChronoUnit.DAYS);
      System.out.println(week2);
      LocalDateTime over2weken = LocalDateTime.now().plus(week2);
      System.out.println(over2weken);
    }

    @Test
    public void testUnsupportedTemporalTypeException(){
        expectedException.expect(UnsupportedTemporalTypeException.class);
        expectedException.expectMessage("Unit must not have an estimated duration");
        Duration.of(1, ChronoUnit.WEEKS);
    }


    @Test
    public void testDurationMoreDays(){
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime later =  LocalDateTime.now().plusDays(400).
                plusHours(3).plusMinutes(15).plusSeconds(30);
        Duration duration = Duration.between(now, later);
        System.out.println(duration);
        assertTrue(duration.toString().startsWith("PT9603H15M30"));


        assertEquals(400,  duration.toDays());
        assertEquals(400*24+3,  duration.toHours());
        assertEquals((400*24+3)*60+15,  duration.toMinutes());
        assertEquals(((400*24+3)*60+15)*60+30,  duration.getSeconds());
    }

    @Test
    public void durationByLocalTime(){
        LocalTime now = LocalTime.now().minusHours(30);
        LocalTime later =  now.plusHours(3).plusMinutes(15).plusSeconds(30).plusNanos(1000);
        Duration duration = Duration.between(now, later);
        assertTrue(duration.toString().startsWith("PT3H15M30"));

        assertEquals(0,  duration.toDays());
        assertEquals(3,  duration.toHours());
        assertEquals(3*60+15,  duration.toMinutes());
        assertEquals((3*60+15)*60+30,  duration.getSeconds());
    }


    @Test
    public void durationByMixed(){
        expectedException.expect(DateTimeException.class);
        expectedException.expectMessage("Unable to obtain LocalDateTime from TemporalAccessor");
        LocalDateTime now = LocalDateTime.now().minusHours(30);
        LocalTime later =  LocalTime.now().plusHours(3).plusMinutes(15)
                .plusSeconds(30).plusNanos(1000);
        Duration.between(now, later);
    }

    @Test
    public void testDurationWithLocalTime(){
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime later =  LocalDateTime.now().plusDays(1).plusHours(3).plusMinutes(15).plusSeconds(30);
        Duration duration = Duration.between(now, later);
        System.out.println(duration);
        assertTrue(duration.toString().startsWith("PT27H15M30"));

    }
}

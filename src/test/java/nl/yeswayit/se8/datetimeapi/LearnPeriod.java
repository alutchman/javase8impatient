package nl.yeswayit.se8.datetimeapi;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.time.temporal.UnsupportedTemporalTypeException;
import java.util.Calendar;

import static org.junit.Assert.assertEquals;

public class LearnPeriod {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testDaysTillChristmas(){
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.MONTH) == 12 && calendar.get(Calendar.DAY_OF_MONTH) > 25 ? calendar.get(Calendar.YEAR)+1
                : calendar.get(Calendar.YEAR);
        LocalDate kerst = LocalDate.of(year, 12, 25);
        Period totKerst2020 = Period.between(LocalDate.now(), kerst);
        System.out.println(totKerst2020);
    }

    @Test
    public void testDaysTillChristmasPeriod(){
        int testYear = 2020;
        LocalDate kerst = LocalDate.of(testYear, 12, 25);
        Period totKerst2021 = Period.between(LocalDate.parse( testYear+"-01-26"), kerst);
        assertEquals("P10M29D",totKerst2021.toString());

        assertEquals(0,totKerst2021.get(ChronoUnit.YEARS));
        assertEquals(10,totKerst2021.get(ChronoUnit.MONTHS));
        assertEquals(29,totKerst2021.get(ChronoUnit.DAYS));
    }

    @Test
    public void testNewDate(){
        assertEquals("P14D", Period.ofWeeks(2).toString());
        assertEquals("P42D", Period.ofDays(42).toString());
        assertEquals("P2M", Period.ofMonths(2).toString());
        assertEquals("P2Y", Period.ofYears(2).toString());
        assertEquals(2, Period.ofYears(2).get(ChronoUnit.YEARS));
    }

    @Test
    public void testUnsupportedTemporalTypeException(){
        expectedException.expect(UnsupportedTemporalTypeException.class);
        expectedException.expectMessage("Unsupported unit: Hours");
        Period testperiod = Period.parse("P10M29D");
        testperiod.get(ChronoUnit.HOURS);
    }

    @Test
    public void showPeriodByLocalDate(){
        LocalDate start = LocalDate.of(2019, 10, 15);
        LocalDate end = LocalDate.of(2019, 12, 25);
        Period totKerst2020 = Period.between(start, end);
        System.out.println(totKerst2020); //P2M10D
        assertEquals(0,totKerst2020.get(ChronoUnit.YEARS));
        assertEquals(2,totKerst2020.get(ChronoUnit.MONTHS));
        assertEquals(10,totKerst2020.get(ChronoUnit.DAYS));
    }


}

package nl.yeswayit.se8.datetimeapi;

import org.junit.Test;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class ClassicDate {

    @Test
    public void conversions(){
        Date utilDate = new Date();
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

        System.out.println("sqlDate: "+ sqlDate.toString());
        System.out.println("sqlDateFromLocalDate: "+java.sql.Date.valueOf(LocalDate.now()));
        System.out.println("utilDate: "+ utilDate.toString());
        System.out.println("sqlDate.toLocalDate(): "+ sqlDate.toLocalDate());
        System.out.println("utilDate.toInstant(): "+  utilDate.toInstant());

    }
}

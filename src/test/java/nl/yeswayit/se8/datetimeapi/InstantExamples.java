package nl.yeswayit.se8.datetimeapi;

import org.junit.Test;

import java.time.Instant;
import java.util.Date;

public class InstantExamples {

    @Test
    public void showInstantOptions(){
        Date utilDate = new Date();
        Instant instant = utilDate.toInstant();
        Date backFromInstant = Date.from(instant);
        java.sql.Date sqlDate = new java.sql.Date(Date.from(instant).getTime());
        System.out.println("util date = "+backFromInstant);
        System.out.println("instant date = "+instant);
        System.out.println("sql date = "+sqlDate);
    }
}

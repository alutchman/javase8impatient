package nl.yeswayit.se8.datetimeapi;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.UnsupportedTemporalTypeException;

import static org.junit.Assert.assertEquals;

public class LearnLocalTime {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void basicTest(){
        LocalTime now  = LocalTime.now(Clock.systemDefaultZone());
        System.out.println("LocalTime.now(Clock.systemDefaultZone()) :"+  LocalTime.now(Clock.systemDefaultZone()));
        System.out.println("now.plus(3, ChronoUnit.HOURS)) :"+  now.plus(3, ChronoUnit.HOURS));
        System.out.println("now.minusHours(2) :"+  now.minusHours(2));

    }

    @Test
    public void testPlusUnhappyFlow() {
        expectedException.expect(UnsupportedTemporalTypeException.class);
        expectedException.expectMessage("Unsupported unit: Days");
        LocalTime.now(Clock.systemDefaultZone()).plus(10, ChronoUnit.DAYS);
    }

    @Test
    public void testSecondsOfDay() {
        assertEquals("00:00:59",LocalTime.ofSecondOfDay(59).toString());
        assertEquals("01:00",LocalTime.ofSecondOfDay(3600).toString());
        assertEquals("00:01:02",LocalTime.ofSecondOfDay(62).toString());

        assertEquals("01:01",LocalTime.ofSecondOfDay(3660).toString());
        assertEquals("02:02",LocalTime.ofSecondOfDay(3660*2).toString());
        assertEquals("03:03:20",LocalTime.ofSecondOfDay(3660*3+ 20).toString());
    }

    @Test
    public void testTimeZones(){
        System.out.println("USA Eastern time = " + LocalTime.now(ZoneId.of("US/Eastern")));
        System.out.println("Europe/Amsterdam time = " + LocalTime.now(ZoneId.of("Europe/Amsterdam")));
        System.out.println("Europe/Istanbul time = " + LocalTime.now(ZoneId.of("Europe/Istanbul")));
        System.out.println("Europe/Bucharest time = " + LocalTime.now(ZoneId.of("Europe/Bucharest")));
        System.out.println("Europe/Warsaw time = " + LocalTime.now(ZoneId.of("Europe/Warsaw")));
    }

    @Test
    public void testParse(){
        assertEquals("02:30:30", LocalTime.parse("02:30:30").toString() );
    }

    @Test
    public void getParts(){
        LocalTime localTime  = LocalTime.now(Clock.systemDefaultZone());
        System.out.println("localTime.getLong(ChronoField.HOUR_OF_DAY): "+
                localTime.getLong(ChronoField.HOUR_OF_DAY));
        System.out.println("localTime.getLong(ChronoField.MINUTE_OF_DAY): "+
                localTime.getLong(ChronoField.MINUTE_OF_DAY));
        System.out.println("localTime.getLong(ChronoField.MINUTE_OF_HOUR): "+
                localTime.getLong(ChronoField.MINUTE_OF_HOUR));
        System.out.println("localTime.getLong(ChronoField.SECOND_OF_MINUTE): "+
                localTime.getLong(ChronoField.SECOND_OF_MINUTE));
        System.out.println("localTime.getLong(ChronoField.SECOND_OF_DAY): "+
                localTime.getLong(ChronoField.SECOND_OF_DAY));
        System.out.println("localTime.getHour(): "+
                localTime.getHour());
        System.out.println("localTime.getMinute(): "+
                localTime.getMinute());
        System.out.println("localTime.getSecond(): "+
                localTime.getSecond());
    }

}

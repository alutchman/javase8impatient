package nl.yeswayit.se8.utils;

public class OtherStringHandler {
    public void printUpperCase(String input){
        System.out.println(input.toUpperCase());
    }

    public void printLowerCase(String input){
        System.out.println(input.toLowerCase());
    }
}

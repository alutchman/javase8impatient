package nl.yeswayit.se8.utils;

import lombok.Data;

import java.util.Objects;

@Data
public class FamilyMember {
    private String lastName;
    private String firstName;
    private double income;

    public FamilyMember(String lastName, String firstName, double income) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.income = income;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FamilyMember that = (FamilyMember) o;
        return getLastName().equals(that.getLastName()) &&
                getFirstName().equals(that.getFirstName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLastName());
    }
}

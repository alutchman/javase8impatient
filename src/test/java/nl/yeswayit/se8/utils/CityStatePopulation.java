package nl.yeswayit.se8.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@AllArgsConstructor
@Data
public class CityStatePopulation {
    private String city;
    private String state;
    private int population;
}

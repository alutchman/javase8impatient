package nl.yeswayit.se8.utils;

import lombok.Data;
import java.io.Serializable;


@Data
public class Person implements Serializable {
    private Integer id;
    private final String firstName;
    private final String lastName;
    private final int birthYear;
    private final String country;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return getBirthYear() == person.getBirthYear() &&
                getFirstName().equals(person.getFirstName()) &&
                getLastName().equals(person.getLastName()) &&
                getCountry().equals(person.getCountry());
    }

    @Override
    public int hashCode() {
        return getLastName().hashCode()*1000+ getFirstName().hashCode();
    }

    public static final class Builder{
        private static int id =1;
        private String firstName  = "-n/a-";
        private String lastName;
        private int birthYear;
        private String country = "-Unknown-";

        public static void reset() {
            id =1;
        }

        public Builder withLastName(String value){
            lastName = value;
            return this;
        }

        public Builder withFirstName(String value){
            firstName = value;
            return this;
        }

        public Builder withCountry(String value){
            country = value;
            return this;
        }

        public Builder withBirthYear(int value){
            birthYear = value;
            return this;
        }

        public final Person build(){
            Person result  = new Person(firstName,lastName,birthYear,country);
            result.setId(id++);
            return result;
        }

    }
}

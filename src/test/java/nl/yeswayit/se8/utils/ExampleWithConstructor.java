package nl.yeswayit.se8.utils;

public class ExampleWithConstructor {
    String label;

    public ExampleWithConstructor(String label){
        this.label = label;

        System.out.println("ExampleWithConstructor call with label: "+ label);

    }

    @Override
    public String toString(){
        return label.toUpperCase();
    }

    public boolean handleIt() {
        return this.label.startsWith("first");
    }
}

package nl.yeswayit.se8.utils;

import lombok.Data;

@Data
public class Greeter {
    public final Person person;

    public String greetByLastName(){
        return "Hello "+ person.getLastName();
    }

    public String greetByFirstName(){
        return "Hello "+ person.getFirstName();
    }

    public String greetByBirthYear(){
        return "Hello fellow born in "+ person.getBirthYear();
    }

    public String greetByCitizen(){
        return "Hello citizen of "+ person.getCountry();
    }


}

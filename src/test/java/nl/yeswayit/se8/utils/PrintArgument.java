package nl.yeswayit.se8.utils;

public interface PrintArgument {
    String showData(Object data);
}

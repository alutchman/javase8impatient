package nl.yeswayit.se8.utils;

public interface Sayable{
    void say(int i);
}

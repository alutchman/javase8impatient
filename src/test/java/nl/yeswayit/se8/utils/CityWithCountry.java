package nl.yeswayit.se8.utils;

import lombok.Data;

@Data
public class CityWithCountry {
    private String city;
    private String country;


    public CityWithCountry(String city, String country) {
        this.city = city;
        this.country = country;
    }
}

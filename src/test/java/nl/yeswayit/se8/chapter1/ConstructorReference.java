package nl.yeswayit.se8.chapter1;

import nl.yeswayit.se8.utils.ExampleWithConstructor;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RunWith(MockitoJUnitRunner.class)
public class ConstructorReference {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();


    @Test
    public void example1(){
        List<String> labels = new ArrayList<>();
        String[] labelsx = {"first label", "second label", "third label"};
        labels.addAll(Stream.of(labelsx).collect(Collectors.toList()));

        Stream<ExampleWithConstructor> streamtest = labels.stream().map(ExampleWithConstructor::new);
        List<ExampleWithConstructor> lst = streamtest.collect(Collectors.toList());
        System.out.println(lst);

        streamtest = labels.stream().map(ExampleWithConstructor::new);
        ExampleWithConstructor[] withArrays = streamtest.toArray(ExampleWithConstructor[]::new);
        System.out.println(withArrays[0]);
    }


    @Test
    public void example2UnhappyFlow(){
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage("stream has already been operated upon or closed");

        List<String> labels = new ArrayList<>();
        String[] labelsx = {"first label", "second label", "third label"};
        labels.addAll(Stream.of(labelsx).collect(Collectors.toList()));

        Stream<ExampleWithConstructor> streamtest = labels.stream().map(ExampleWithConstructor::new);
        streamtest.collect(Collectors.toList());


        ExampleWithConstructor[] withArrays = streamtest.toArray(ExampleWithConstructor[]::new);
        System.out.println(withArrays[0]);
    }
}

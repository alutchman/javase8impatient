package nl.yeswayit.se8.chapter1;

import nl.yeswayit.se8.utils.PersonItem;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RunWith(MockitoJUnitRunner.class)
public class VariableScope {
    private static final String USERSMAP     = "c:\\users\\";
    private static final String BASEMAP      = USERSMAP+"arwlu\\";
    private static final String DOWNLOADMAP  = BASEMAP+"Downloads";

    @Test
    public void testRunnable() {
        new Thread(this::handleRunnable).start();
        repeatMessage("Testing variable scope 1", 3);
        repeatMessage("Testing variable scope 2", 2);
    }

    private void handleRunnable(){
        for (int i =0; i < 10; i++) {
            System.out.println(i);
            Thread.yield();
        }
    }

    /**
     * Variable remains available..
     *
     * @param text
     * @param count
     */
    public static void repeatMessage(String text, int count){
        Runnable r = () -> {
           for (int i=0; i < count; i++) {
               System.out.println(text );
               Thread.yield();
           }
        };
        new Thread(r).start();
    }

    @Test
    public void testDefaultMethods(){
        PersonItem personItem = this::getIdForPerson;

        System.out.println(personItem.getid());
        System.out.println(personItem.getName());
    }

    public long getIdForPerson(){
        return 10l;
    }

    @Test
    public void testExcercise2() throws IOException {
        File file = new File(USERSMAP);

        File[] dirs = file.listFiles(pathname ->  pathname.isDirectory() && !pathname.isHidden());
/*        File[] dirs = file.listFiles(new FileFilter(){
            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory() && !pathname.isHidden();
            }
        });*/

        List<File> dirs2 = Stream.of(file.listFiles(x -> !x.isHidden() && x.isDirectory())).collect(Collectors.toList());
        System.out.println(dirs2.stream().map(File::getName).collect(Collectors.joining(", ")));
        System.out.println(Stream.of(file.listFiles(this::filterFile)).map(File::getName).collect(Collectors.joining(" - ")));

        List<File> dirsList =
                Stream.of(file.listFiles(x -> x.isDirectory() && !x.isHidden())).collect(Collectors.toList());
        System.out.println(dirsList.stream().map(File::getName).map(String::toUpperCase).collect(Collectors.joining("; ")));

        //---Excercise 3
        file = new File(DOWNLOADMAP);
        dirs2 = Stream.of(file.listFiles(x -> !x.isHidden() && !x.isDirectory())).collect(Collectors.toList());
        System.out.println("Aleen Files: "+dirs2.stream().map(File::getName).filter(x -> x.endsWith(".pdf")).collect(Collectors.joining(", ")));
    }

    public boolean filterFile(File pathname){
        return pathname.isDirectory() && !pathname.isHidden();
    }

    @Test
    public void testExcercise3() throws IOException {
        File file = new File(BASEMAP);
        file = new File(DOWNLOADMAP);
        List<File> dirs2 = Stream.of(file.listFiles(x -> !x.isHidden() && !x.isDirectory())).collect(Collectors.toList());
        System.out.println(dirs2.stream().map(File::getName).filter(x -> x.endsWith(".pdf")).collect(Collectors.joining(", ")));
    }

    @Test
    public void testExcercise4() throws IOException {
        File file = new File(DOWNLOADMAP);
        List<File> dirs2 = Stream.of(file.listFiles()).filter(x -> !x.isHidden()).collect(Collectors.toList());

        List<String> onlyDirs = dirs2.stream().filter(File::isDirectory).map(File::getName).sorted(this::compareStringCaseIgnore).collect(Collectors.toList());
        List<String> onlyNonDirs = dirs2.stream().filter(File::isFile).map(File::getName).sorted(this::compareStringCaseIgnore).collect(Collectors.toList());

        System.out.println("Only dirs = "+onlyDirs);
        System.out.println("OnlyNon Dirs = "+onlyNonDirs);
        List<String> combinedStream = Stream.concat(
                onlyDirs.stream(),
                onlyNonDirs.stream()).collect(Collectors.toList());
        System.out.println("AlleBei: "+ combinedStream);
    }

    public int compareStringCaseIgnore(String x, String y) {
        x = x.toUpperCase();
        y = y.toUpperCase();
        return x.compareTo(y);
    }
}

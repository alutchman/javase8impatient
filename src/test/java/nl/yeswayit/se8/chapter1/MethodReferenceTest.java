package nl.yeswayit.se8.chapter1;

import nl.yeswayit.se8.utils.Greeter;

import nl.yeswayit.se8.utils.OtherStringHandler;
import nl.yeswayit.se8.utils.Person;
import nl.yeswayit.se8.utils.Sayable;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

@RunWith(MockitoJUnitRunner.class)
public class MethodReferenceTest {

    @Test
    public void testInstanceMethod(){
        Person person1 = new Person.Builder().withFirstName("Donald").withLastName("Duck").
                withBirthYear(1900).withCountry("USA").build();
        Greeter greeter = new Greeter(person1);

        Consumer<String> c = System.out::println;

        c.accept(greeter.greetByCitizen());
        c.accept(greeter.greetByBirthYear());

        c = this::handleString;
        c.accept(greeter.greetByFirstName());

        c = MethodReferenceTest::handleStaticString;
        c.accept(greeter.greetByLastName());

        OtherStringHandler otherStringHandler = new OtherStringHandler();
        c = otherStringHandler::printLowerCase;
        c.accept(greeter.greetByLastName());
        c = otherStringHandler::printUpperCase;
        c.accept(greeter.greetByLastName());
    }

    public void handleString(String input) {
        System.out.println("Local instance method calling: "+input);
    }

    public static void handleStaticString(String input) {
        System.out.println("Local static method calling: "+input);
    }

    @Test
    public void testClassInstanceMethod(){
        List<Person> data = new ArrayList<Person>();
        data.add(new Person.Builder().withFirstName("Donald").withLastName("Duck").
                withBirthYear(1900).withCountry("USA").build());
        data.add(new Person.Builder().withFirstName("Angela").withLastName("Lastwoman").
                withBirthYear(2000).withCountry("Argentina").build());
        data.add(new Person.Builder().withFirstName("Sound").withLastName("Ofmusic").
                withBirthYear(1966).withCountry("USA").build());
        data.add(new Person.Builder().withFirstName("Luitenant").withLastName("Columbo").
                withBirthYear(1970).withCountry("Netherlands").build());

        Collections.sort(data, Comparator.comparing(Person::getFirstName));
        System.out.println("Sorted by getFirstName: " + data);
        Collections.sort(data,Comparator.comparing(Person::getBirthYear));
        System.out.println("Sorted by getBirthYear: " + data);


        String[] stringArray = { "Barbara", "James", "Mary", "John",
                "Patricia", "Robert", "Michael", "Linda" };
        Arrays.sort(stringArray, String::compareToIgnoreCase);
        Stream.of(stringArray).limit(3).forEach(System.out::println);
    }

    @Test
    public void testAllTogether(){
        //===object instance method
        Sayable sayable = this::saySomethingObjectInstance;
        sayable.say(12);

        sayable = MethodReferenceTest::saySomethingclassInstance;
        sayable.say(14);

    }


    public void saySomethingObjectInstance(int i){
        System.out.println("Hello, this is instance method. number passed: "+ i);
    }

    public static void saySomethingclassInstance(int i){
        System.out.println("Hello, this is class method. number passed: "+ i);
    }
}


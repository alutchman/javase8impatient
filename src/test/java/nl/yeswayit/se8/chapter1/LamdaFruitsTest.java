package nl.yeswayit.se8.chapter1;


import nl.yeswayit.se8.utils.LengthTextComaparator;
import nl.yeswayit.se8.utils.PrintArgument;
import nl.yeswayit.se8.utils.Worker;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class LamdaFruitsTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testRunnable(){
        Worker w = new Worker();
        Thread thread =   new Thread(w);
        thread.setPriority(1);

        thread.start();
        System.out.println("Thread: " +thread.getId());
    }

    @Test
    public void testStringCompare(){
        List<String> data = new ArrayList<String>();
        data.addAll(Arrays.asList("Be quiet","X man","A larger text"));
        System.out.println("Unsorted: " + data);
        Collections.sort(data);
        System.out.println("Sorted Alhpabetically: " + data);
        Collections.sort(data,new LengthTextComaparator());
        System.out.println("Sorted by length: " + data);
    }

    @Test
    public void testRunnableLambda(){

        Thread thread =   new Thread(() -> runThread());
        thread.setPriority(1);


        thread.start();
        System.out.println("Thread: " +thread.getId());
    }

    public void runThread() {
        for(int i=0; i < 20; i++){
            System.out.println("Iteration number "+ i);
        }
    }

    @Test
    public void testStringCompareLambda(){
        List<String> data = new ArrayList<String>();
        data.addAll(Arrays.asList("Be quiet","X man","A larger text"));
        System.out.println("Unsorted: " + data);
        Collections.sort(data);
        System.out.println("Sorted Alhpabetically: " + data);

        /*
         is same as
            Collections.sort(data,(first,second)->{return Integer.compare(first.length(),second.length());});
         */
        Collections.sort(data,Comparator.comparing(String::length));

        System.out.println("Sorted by length: " + data);
    }


    /**
     * (Object datax) -> datax.getClass().getCanonicalName()+":"+datax.toString();
     *
     * is same as
     *
     * (datax) -> datax.getClass().getCanonicalName()+":"+datax.toString();
     *
     * and
     *
     * datax -> datax.getClass().getCanonicalName()+":"+datax.toString();
     */
    @Test
    public void testInterfaceImplementation(){
        PrintArgument pi = datax -> showObjectInfo(datax);
        // which could also be written as : PrintArgument pi = this::showObjectInfo;

        System.out.println(pi.showData("simple text"));
        System.out.println(pi.showData(10l));
        System.out.println(pi.showData(104));
        System.out.println(pi.showData(0.55f));
        System.out.println(pi.showData(20000.234));


    }

    private String  showObjectInfo(Object datax){
        return datax.getClass().getCanonicalName()+":"+datax.toString();
    }

    /**
     * Unnecesary using the final
     */
    @Test
    public void testInterfaceWithModifiers(){
        PrintArgument pi = (Object datax) -> datax.getClass().getCanonicalName()+":"+datax.toString();
        System.out.println(pi.showData("simple text"));
        System.out.println(pi.showData(10l));
        System.out.println(pi.showData(104));
        System.out.println(pi.showData(0.55f));
        System.out.println(pi.showData(20000.234));
    }



}

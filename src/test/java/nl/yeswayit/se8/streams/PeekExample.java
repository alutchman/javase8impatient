package nl.yeswayit.se8.streams;

import lombok.Data;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class PeekExample {
    @Test
    public void peektest(){
        List<Movie> movies = Arrays.asList(
                new Movie("Titanic", Movie.Genre.DRAMA, 'U'),
                new Movie("Psycho", Movie.Genre.THRILLER, 'U'),
                new Movie("Oldboy", Movie.Genre.THRILLER, 'R'),
                new Movie("Shining", Movie.Genre.HORROR, 'U')
        );

        movies.stream()
                .filter(mov->mov.getRating()=='R')
                .peek(mov->System.out.println(mov.getName()))
                .map(mov->mov.getName())
                .forEach(System.out::println);
    }

    @Test
    public void niooption(){
        Path p1 = Paths.get("c:\\..\\temp\\test.txt");
        System.out.println(p1.normalize().toUri());
    }

}


@Data
class Movie {
    enum Genre  {DRAMA, THRILLER, HORROR, ACTION };

    private Genre genre;
    private String name;
    private char rating = 'R';

    Movie(String name, Genre genre, char rating) {
        this.name = name; this.genre = genre; this.rating = rating;
    }
    //accessors not shown

}
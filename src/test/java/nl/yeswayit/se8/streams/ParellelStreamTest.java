package nl.yeswayit.se8.streams;

import lombok.Synchronized;
import nl.yeswayit.se8.utils.CityStatePopulation;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ParellelStreamTest {
    @Test
    public void testParallelItems(){
        List<CityStatePopulation> lstData =
        Stream.of(new CityStatePopulation("New York","New York", 1000000),
                new CityStatePopulation("Boston", "New York", 50000),
                new CityStatePopulation("Miami", "Florida", 1345000),
                new CityStatePopulation("Tampa", "Florida", 120000),
                new CityStatePopulation("Gainesville", "Florida",124921)).
                collect(Collectors.toList());
        ConcurrentMap<String,List<CityStatePopulation>> result0 = lstData.stream().parallel().collect(
                Collectors.groupingByConcurrent(CityStatePopulation::getState)
        );
        System.out.println(result0);

        lstData.stream().parallel().map(this::addDigit2String).forEach(System.out::println);

    }

    @Synchronized
    public String addDigit2String(CityStatePopulation cityStatePopulation){
        return cityStatePopulation.getState()+":"+cityStatePopulation.getCity();
    }

    @Synchronized
    public void printItem(Object item){
        System.out.println(item);
    }
}

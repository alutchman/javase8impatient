package nl.yeswayit.se8.streams;

import nl.yeswayit.se8.utils.Person;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

public class CollectingIntoMaps {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private List<Person> data = new ArrayList<>();

    @Before
    public void init(){
        Person.Builder.reset();
        data.add(new Person.Builder().withFirstName("Donald").withLastName("Trump").
                withBirthYear(1900).withCountry("USA").build());
        data.add(new Person.Builder().withFirstName("Nancy").withLastName("Reagan").
                withBirthYear(2000).withCountry("Argentina").build());
        data.add(new Person.Builder().withFirstName("Ivana").withLastName("Trump").
                withBirthYear(1966).withCountry("USA").build());
        data.add(new Person.Builder().withFirstName("Ronald").withLastName("Reagan").
                withBirthYear(1970).withCountry("Netherlands").build());
    }

    @Test
    public void testIntStringMap(){
        Map<Integer,String> personMap = data.stream().collect(Collectors.toMap(Person::getId, Person::getLastName));
        System.out.println(personMap);
    }

    @Test
    public void testIntObjectMap(){
        Map<Integer,Person> personMap = data.stream().collect(Collectors.toMap(Person::getId, Function.identity()));
        System.out.println(personMap);
    }

    @Test
    public void testMapIllegalState(){
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage("Duplicate key Person(id=1");
        Map<String,Person> personMap = data.stream().collect(Collectors.toMap(Person::getLastName, Function.identity()));
        System.out.println(personMap);
    }

    @Test
    public void testMapIntoSets(){
        Stream<Person> strPerson = data.stream();
        Map<String, Set<Person>> personMap =
        strPerson.collect(Collectors.groupingBy(
                Person::getLastName,
                Collectors.mapping(
                        Function.identity(),
                        toSet()
                )
        ));
        System.out.println(personMap);
    }

    @Test
    public void tesMapToSetMultibelMappings(){
        Stream<Person> personStream = data.stream();
        Map<String, Set<String>>  personMap =
                personStream.collect(Collectors.toMap(
                        l -> l.getLastName(),
                        l -> Collections.singleton(l.getFirstName()),
                        (a,b) -> {
                            Set<String> r = new HashSet<>(a);
                            r.addAll(b);
                            return r;
                        }
                ));
        System.out.println(personMap);

        personStream = data.stream();
        Map<String, Set<Person>>  personMap2 =
                personStream.collect(Collectors.toMap(
                        l -> l.getLastName(),
                        l -> Collections.singleton(l),
                        (a,b) -> {
                            Set<Person> r = new HashSet<>(a);
                            r.addAll(b);
                            return r;
                        }
                ));
        System.out.println(personMap2);
    }

    @Test
    public void testMapGroupingToListAndSet(){
        Stream<Person> personStream = data.stream();
        Map<String,List<Person>> mapPerson = personStream.collect(Collectors.groupingBy(Person::getLastName));
        System.out.println(mapPerson);

        personStream = data.stream();
        Map<String,Set<Person>> mapPersonSet = personStream.collect(Collectors.groupingBy(Person::getLastName,Collectors.toSet()));
        System.out.println(mapPersonSet);

    }
}

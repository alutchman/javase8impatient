package nl.yeswayit.se8.streams;

import nl.yeswayit.se8.utils.Person;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class ConfluenceStreamTest {
    private final ClassLoader classLoader = getClass().getClassLoader();
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private List<Person> data = new ArrayList<Person>();

    @Before
    public void before(){
        Person.Builder.reset();
        data = new ArrayList<Person>();
        data.add(new Person.Builder().withFirstName("Donald").withLastName("Duck").
                withBirthYear(1900).withCountry("USA").build());
        data.add(new Person.Builder().withFirstName("Angela").withLastName("Lastwoman").
                withBirthYear(2000).withCountry("Argentina").build());
        data.add(new Person.Builder().withFirstName("Sound").withLastName("Ofmusic").
                withBirthYear(1966).withCountry("USA").build());
        data.add(new Person.Builder().withFirstName("Luitenant").withLastName("Columbo").
                withBirthYear(1970).withCountry("Netherlands").build());
    }


    @Test
    public void testCollectMap(){
        Map<Integer,Person> idToPerson = data.stream().collect(Collectors.toMap(
                Person::getId,
                Function.identity(),
                (exist, newval) -> {throw new IllegalStateException("Duplicates");},
                TreeMap::new
        ));
        data.stream().map(Person::getId).forEach(System.out::println);

        assertEquals(idToPerson.get(1).getFirstName(),"Donald"  );
        assertEquals(idToPerson.get(4).getFirstName(),"Luitenant"  );
    }

    @Test
    public void testCollectMapBadFlow(){
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage("Duplicate id: 1");
        data.get(1).setId(1);
        data.stream().collect(Collectors.toMap(
                Person::getId,
                Function.identity(),
                (exist, newval) -> {throw new IllegalStateException("Duplicate id: " + exist.getId());},
                TreeMap::new
        ));
    }

    @Test
    public void testTryWithResources(){

        File aFile = new File(classLoader.getResource("J8Upgrade.txt").getFile());
        try (Stream<String> lines = Files.lines(aFile.toPath())) {
            lines.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testStreamCreate(){
        Stream<String> songs=Stream.of("song1" , "song2", "song3");
        Stream<String> stil = Stream.empty();

        String[] array = {"Tekst a", "Tekst b", "Tekst c", "Tekst d", "Tekst e"};
        Stream<String> part =  Arrays.stream(array, 1, 3);
        part.forEach(System.out::println);
    }

    @Test
    public void testGenerate(){
        Stream<String> echoes = Stream.generate(()-> "echo").limit(5);
        Stream<Double> randoms = Stream.generate(Math::random).limit(4);

        echoes.forEach(System.out::println);
        randoms.forEach(System.out::println);

        String data = "Een zin met meerdere woorden";
        Stream<String> words = Pattern.compile("[\\P{L}]+").splitAsStream(data);
        words.forEach(System.out::println);

        File aFile = new File(classLoader.getResource("J8Upgrade.txt").getFile());
        try (Stream<String> lines = Files.lines(aFile.toPath())) {
            lines.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testFilter(){
        String zin = "Morgen zal het kouder zijn dan vandaag";
        Stream<String> words = Pattern.compile("[\\P{L}]+").splitAsStream(zin);
        Stream<String> longOne = words.filter( w -> w.length() > 5);
        longOne.forEach(System.out::println);
    }

    @Test
    public void testMap(){
        String zin = "Morgen Zal het Kouder Zijn Dan Vandaag";
        Stream<String> words = Pattern.compile("[\\P{L}]+").splitAsStream(zin);
        Stream<Character> firstChars = words.map(s -> s.charAt(0));
        firstChars.forEach(System.out::print);

        System.out.println();

        words = Pattern.compile("[\\P{L}]+").splitAsStream(zin);
        Stream<String> lower = words.map(String::toLowerCase);
        lower.forEach(System.out::println);
    }

    @Test
    public void testFlatMapping(){
        String sentence = "Dit is een zin met meerdere woorden";
        Stream<String> wordsStream = Stream.of(sentence.split("[\\P{L}]+"));

        wordsStream.forEach(System.out::println);
        wordsStream = Stream.of(sentence.split("[\\P{L}]+"));

        Stream<Stream<Character>> nonFlat = wordsStream.map(this::characterStream);
        List<Stream<Character>> wordsCharsStreams =  nonFlat.collect(Collectors.toList());
        wordsCharsStreams.forEach(this::printSingleCharstr);

        wordsStream = Stream.of(sentence.split("[\\P{L}]+"));
        Stream<Character> aFlatMap = wordsStream.flatMap(w -> characterStream(sentence));
        printSingleCharstr(aFlatMap);
    }

    public void printSingleCharstr(Stream<Character> charStream) {
        List<Character> lst = charStream.collect(Collectors.toList());
        System.out.println(lst);
    }

    public Stream<Character> characterStream(String sentence) {
        return sentence.chars().mapToObj( c-> (char)c);
    }


    @Test
    public void testNoFlatMap(){
        String zin = "Morgen Zal het Kouder Zijn Dan Vandaag";
        Stream<String> wordsStream = Stream.of(zin.split("[\\P{L}]+"));

        Stream<Stream<Character>> nonFlat = wordsStream.
                map(w -> w.chars().mapToObj( c-> (char)c));
        List<List<Character>> reeksen = nonFlat.
                map(w -> w.collect(Collectors.toList())).collect(Collectors.toList());
        reeksen.stream().forEach(System.out::println);
    }

    @Test
    public void testFlatMap() {
        String zin = "Morgen Zal het Kouder Zijn Dan Vandaag";
        Stream<String> wordsStream = Stream.of(zin.split("[\\P{L}]+"));

        Stream<Character> flat = wordsStream.flatMap(w -> w.chars().mapToObj( c-> (char)c));
        flat.forEach(System.out::print);
    }

    @Test
    public void testExtracting(){
        Stream<Double> rd = Stream.generate(Math::random).limit(100);
        //Discard first 95 elements
        rd.skip(95).forEach(System.out::println);
    }

    @Test
    public void testConcat(){
        Stream<Character> deel1 = "Worden".chars().mapToObj(x -> (char)x);
        Stream<Character> deel2 = "Gezocht".chars().mapToObj(x -> (char)x);
        Stream.concat(deel1, deel2).forEach(System.out::print);
    }

    @Test
    public void testPeek(){
        Stream.of("one", "two", "three", "four")
                .filter(e -> e.length() > 3)
                .peek(e -> System.out.println("Filtered value: " + e))
                .map(String::toUpperCase)
                .peek(e -> System.out.println("Mapped value: " + e))
                .collect(Collectors.toList());
    }

    @Test
    public void testTransformations(){
        Stream<String> unique = Stream.of("Een" , " Twee", "Drie", "Twee ").map(String::trim).distinct();
        unique.forEach(System.out::println);
    }

    @Test
    public void testSorting(){
        Stream<String> sorted = Stream.of("Go" , "Home", "Now", "Again").sorted(Comparator.comparing(String::length).reversed());
        sorted.forEach(System.out::println);
    }

    @Test
    public void testReductions(){
        Stream<String> str = Stream.of("Go" , "Home", "Get", "Again");
        Optional<String> largest = str.max(String::compareToIgnoreCase);
        if (largest.isPresent()) {
            System.out.println(largest.get());
        }

        str = Stream.of("Go" , "Home", "Get", "Again");
        Optional<String> startsWithG = str.filter(s -> s.startsWith("G")).findFirst();
        //of Optional<String> startsWithG = str.filter(s -> s.startsWith("G")).findAny();
        if (startsWithG.isPresent()) {
            System.out.println(startsWithG.get());
        }

        str = Stream.of("Go" , "Home", "Get", "Again");
        boolean startWithG = str.parallel().anyMatch(s -> s.startsWith("G"));
        System.out.println("Woorden die met G beginnen gevonden? "+startWithG);
    }


}

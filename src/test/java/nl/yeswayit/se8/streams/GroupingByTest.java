package nl.yeswayit.se8.streams;

import nl.yeswayit.se8.utils.CityStatePopulation;
import nl.yeswayit.se8.utils.CityWithCountry;
import nl.yeswayit.se8.utils.FamilyMember;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.DoubleSummaryStatistics;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.averagingDouble;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.maxBy;
import static java.util.stream.Collectors.minBy;
import static java.util.stream.Collectors.summarizingDouble;
import static java.util.stream.Collectors.summarizingInt;
import static java.util.stream.Collectors.summingDouble;
import static java.util.stream.Collectors.summingInt;

public class GroupingByTest {
    private List<CityWithCountry> cityData = new ArrayList<>();

    @Before
    public void init(){
        cityData.add(new CityWithCountry("Rotterdam", "The Netherlands"));
        cityData.add(new CityWithCountry("Amsterdam", "The Netherlands"));
        cityData.add(new CityWithCountry("Utrecht", "The Netherlands"));
        cityData.add(new CityWithCountry("London", "United Kingdom"));
        cityData.add(new CityWithCountry("Southall", "United Kingdom"));
        cityData.add(new CityWithCountry("Birmingham", "United Kingdom"));
        cityData.add(new CityWithCountry("Caracas", "Venezuela" ));
        cityData.add(new CityWithCountry("Valencia", "Venezuela" ));
    }

    @Test
    public void testPartitionByCountry(){
        Map<Boolean,List<CityWithCountry>> dutchArea =
           cityData.stream().collect(Collectors.partitioningBy(l -> l.getCountry().equals("The Netherlands")));

        System.out.println(dutchArea);
    }

    @Test
    public void testIncomeFamilyMembers(){
        List<FamilyMember> familyMembers = new ArrayList<>();
        familyMembers.add(new FamilyMember("Reagan","Nancy",3550.00));
        familyMembers.add(new FamilyMember("Reagan","Bella",2550.00));
        familyMembers.add(new FamilyMember("Reagan","Donny",1550.00));
        familyMembers.add(new FamilyMember("Johnson","Fellow",4550.00));
        familyMembers.add(new FamilyMember("Johnson","Fella",3550.00));

        Map<String, Long> membersCount = familyMembers.stream().collect(
                Collectors.groupingBy(FamilyMember::getLastName, counting())
        );
        System.out.println(membersCount);

        Map<String, Double> membersTotalIncome = familyMembers.stream().collect(
                Collectors.groupingBy(FamilyMember::getLastName, summingDouble(FamilyMember::getIncome))
        );
        System.out.println("Total per Family: "+membersTotalIncome);

        Map<String, Double> memberAverageIncome = familyMembers.stream().collect(
                Collectors.groupingBy(FamilyMember::getLastName, averagingDouble(FamilyMember::getIncome))
        );
        System.out.println("Average per Family: "+memberAverageIncome);

        Map<String, Optional<FamilyMember>> memberMaxincome = familyMembers.stream().collect(
                groupingBy(FamilyMember::getLastName, maxBy(Comparator.comparing(FamilyMember::getIncome))
        ));
        System.out.println("Max income by person per Family: "+memberMaxincome);

        Map<String, Optional<FamilyMember>> memberMinincome = familyMembers.stream().collect(
                groupingBy(FamilyMember::getLastName, minBy(Comparator.comparing(FamilyMember::getIncome))
                ));
        System.out.println("Min income by person per Family: "+memberMinincome);
    }

    @Test
    public void testIt(){
        List<CityStatePopulation> lstData =
        Stream.of(new CityStatePopulation("New York","New York", 1000000),
                new CityStatePopulation("Boston", "New York", 50000),
                new CityStatePopulation("Miami", "Florida", 1345000),
                new CityStatePopulation("Tampa", "Florida", 120000),
                new CityStatePopulation("Gainesville", "Florida",124921)).
                collect(Collectors.toList());
        Map<String,String> stateHasCities = lstData.stream().collect(
                groupingBy(CityStatePopulation::getState,
                            mapping(CityStatePopulation::getCity, joining("-"))));
        System.out.println("stateHasCities: "+stateHasCities);
        Map<String,Integer> stateToCityPopupation = lstData.stream().collect(
                groupingBy(CityStatePopulation::getState, summingInt(CityStatePopulation::getPopulation)));
        System.out.println("stateToCityPopupation: "+stateToCityPopupation);

        Map<String, IntSummaryStatistics> statistics = lstData.stream().collect(
                groupingBy(CityStatePopulation::getState, summarizingInt(CityStatePopulation::getPopulation)));
        System.out.println("statistics : "+ statistics);

    }

}

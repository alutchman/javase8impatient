package nl.yeswayit.se8.streams;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class OptionalTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testInit(){
        Optional<String> empty = Optional.empty();
        assertFalse(empty.isPresent());
    }

    @Test
    public void givenNonNull_whenCreatesNonNullable_thenCorrect() {
        String name = "mycerfification";
        Optional<String> opt = Optional.of(name);
        assertTrue(opt.isPresent());

        System.out.println("Optional 1: "  + opt);

        name = null;
        Optional<String> opt2 = Optional.ofNullable(name);
        assertFalse(opt2.isPresent());
        System.out.println("Optional 2: "  + opt2);
    }

    @Test
    public void exampleOptionalGetTest() {
        String name = "mycerfification";
        Optional<String> opt = Optional.ofNullable(name);
        assertTrue(opt.isPresent());

        System.out.println("Optional 1: "  + opt.get());
    }


    @Test
    public void exampleOptionalOrElse() {
        System.setProperty("alternative","undef");
        String name = "Valid String";
        Optional<String> opt = Optional.ofNullable(name);

        String result  = opt.orElse("Other Value");
        System.out.println("Result when name !=null: "  + result);
        System.out.println("Optional 1: "  + opt);


        name = null;
        Optional<String> opt2 = Optional.ofNullable(name);

        String result2  = opt2.orElse("Null found");
        System.out.println("Result when name =null: "  + result2);
        System.out.println("Optional 2: "  + opt2);
    }

    @Test
    public void exampleOptionalOrElseGet() {
        System.setProperty("alternative","undef");
        String name = "Valid String";
        Optional<String> opt = Optional.ofNullable(name);

        String result  = opt.orElseGet(() -> System.getProperty("alternative"));
        System.out.println("Result when name !=null: "  + result);
        System.out.println("Optional 1: "  + opt);


        name = null;
        Optional<String> opt2 = Optional.ofNullable(name);

        String result2  = opt2.orElseGet(() -> System.getProperty("alternative"));
        System.out.println("Result when name =null: "  + result2);
        System.out.println("Optional 2: "  + opt2);
    }

    @Test
    public void exampleOptionalOrElseThrowHappyFlow() {
        String name = "Valid String";
        Optional<String> opt = Optional.ofNullable(name);

        String result  = opt.orElseThrow(NoSuchElementException::new);
        System.out.println("Result when name !=null: "  + result);
        System.out.println("Optional 1: "  + opt);
    }


    @Test
    public void exampleOptionalOrElseThrowUnHappyFlow() {
        expectedException.expect(NoSuchElementException.class);
        String name = null;
        Optional<String> opt = Optional.ofNullable(name);
        opt.orElseThrow(NoSuchElementException::new);
    }

    @Test
    public void testOptionalIfPresent(){
        List<String> fields = new ArrayList<>();
        fields.addAll(Stream.of("Firstname", "LastName","Birthdate").collect(Collectors.toList()));
        String name = "Woonplaats";
        Optional<String> dataOptional = Optional.ofNullable(name);
        dataOptional.ifPresent(fields::add);

        fields.stream().forEach(System.out::println);

        System.out.println();
        String name2= null;

        dataOptional = Optional.ofNullable(name2);
        dataOptional.ifPresent(fields::add);

        fields.stream().forEach(System.out::println);
    }



    @Test
    public void OptionalWithFlatmapTest(){
        Optional<String> nonEmptyGender = Optional.of("male");
        Optional<String> emptyGender = Optional.empty();

        System.out.println("Non-Empty Optional:: " + nonEmptyGender.map(String::toUpperCase));
        System.out.println("Empty Optional    :: " + emptyGender.map(String::toUpperCase));

        Optional<Optional<String>> nonEmptyOtionalGender = Optional.of(Optional.of("male"));
        System.out.println("Optional value   :: " + nonEmptyOtionalGender);
        System.out.println("Optional.map     :: " + nonEmptyOtionalGender.map(gender -> gender.map(String::toUpperCase)));
        System.out.println("Optional.flatMap :: " + nonEmptyOtionalGender.flatMap(gender -> gender.map(String::toUpperCase)));

    }

    @Test
    public void OptionalWithFlatmapPersonTest(){
        Optional<String> word = Optional.of("apple");
        Optional<Optional<String>> optionalOfOptional = word.map( s -> (s == null) ? Optional.empty() : Optional.of(s.toUpperCase()));
        Optional<String> upperCasedOptional = word.flatMap( s -> (s == null) ? Optional.empty() : Optional.of(s.toUpperCase()));
        assertThat(optionalOfOptional.get().get(), is("APPLE"));
        assertThat(upperCasedOptional.get(), is("APPLE"));

        System.out.println("Optional.map     :: " + optionalOfOptional);
        System.out.println("Optional.flatMap :: " + upperCasedOptional);
    }

    public Optional<Double> squareRoot(Double x){
        return x== null || x < 0 ? Optional.empty() : Optional.of(Math.sqrt(x)) ;
    }

    public Optional<Double> inversie(Double x){
        return x== null || x == 0 ?  Optional.empty() : Optional.of(1/x) ;
    }
}

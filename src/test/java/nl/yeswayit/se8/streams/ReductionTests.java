package nl.yeswayit.se8.streams;

import org.junit.Test;

import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class ReductionTests {
    @Test
    public void reductionTestIntegers(){
        Stream<Integer> getallen = Stream.of(1,2,3,4,5);
        Optional<Integer> result0 = getallen.reduce( (x,y) -> x+y);
        //get the integer directly using the identity 0 for addition
        Integer result1 = Stream.of(1,2,3,4,5).reduce(0, (x,y) -> x+y);
        //is Uquavalent
        Integer result2 =  Stream.of(1,2,3,4,5).reduce(0,Integer::sum);
        assertEquals(Optional.of(15), result0);
        assertEquals(15, result1.intValue());
        assertEquals(15, result2.intValue());
    }

    @Test
    public void reductionTestString(){
        Optional<String> result0 = Stream.of("Duck", "Goes", "Banana").reduce(String::concat);
        String result1 = Stream.of("Duck", "Goes", "Banana").reduce("",String::concat);
        assertEquals(Optional.of("DuckGoesBanana"), result0);
        assertEquals("DuckGoesBanana", result1);
    }
}

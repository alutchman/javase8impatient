package nl.yeswayit.se8.streams;

import nl.yeswayit.se8.utils.CityStatePopulation;
import nl.yeswayit.se8.utils.FamilyMember;
import nl.yeswayit.se8.utils.Person;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.DoubleFunction;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FunctionalInterfacesTest {
    private final Map<Boolean,List<Person>> groups = new HashMap<>();

    @Before
    public void init(){
        groups.clear();
    }

    @Test
    public void supplierTest(){
        List<String> names = new ArrayList<String>();
        names.add("Harry");
        names.add("Daniel");
        names.add("Lucifer");
        names.add("April O' Neil");

        names.stream().forEach((item)-> {
            printNames(()-> item);
        });
    }

    private void printNames(Supplier<String> supplier) {
        System.out.println(supplier.get());
    }

    @Test
    public void supplierExample2(){
        Supplier<Double> randomval = () -> Math.random();
        System.out.println(randomval.get());
        System.out.println(randomval.get());

    }

    @Test
    public void consumerTest(){
        System.out.println("E.g. #1 - Java8 Consumer Example\n");

        Consumer<String> consumer = this::printNames;
        consumer.accept("C++");
        consumer.accept("Java");
        consumer.accept("Python");
        consumer.accept("Ruby On Rails");
    }


    private void printNames(String name) {
        System.out.println(name);
    }

    @Test
    public void biconsumerTest(){
        List<Person> data = new ArrayList<>();

        Person.Builder.reset();
        data.add(new Person.Builder().withFirstName("Donald").withLastName("Trump").
                withBirthYear(1900).withCountry("USA").build());
        data.add(new Person.Builder().withFirstName("Ola").withLastName("Guapa").
                withBirthYear(2000).withCountry("Argentina").build());
        data.add(new Person.Builder().withFirstName("Ivana").withLastName("Trump").
                withBirthYear(1966).withCountry("USA").build());
        data.add(new Person.Builder().withFirstName("Jan").withLastName("Harthog").
                withBirthYear(1970).withCountry("Netherlands").build());

        BiConsumer<Person, Boolean> persFamilyHandler = this::couplePersonToFamily;
        data.stream().forEach(person ->  persFamilyHandler.accept(person,person.getCountry().equalsIgnoreCase("USA")));
        groups.keySet().stream().forEach(x -> this.printForTrueOrFalse(x, groups.get(x)));
    }

    public void couplePersonToFamily(Person person, Boolean fromUsa){
        if (!groups.containsKey(fromUsa)){
            groups.put(fromUsa,new ArrayList<>());
        }
        groups.get(fromUsa).add(person);
    }

    public void printForTrueOrFalse(boolean isUsa, List<Person> groupofPers){
        System.out.println("USA = "+ isUsa);
        groupofPers.stream().forEach(System.out::println);
    }

    @Test
    public void testPerdicate(){
        Predicate<Integer> lesserthan = i -> (i < 100);
        for (int i= 90; i < 115; i+=5 ){
            System.out.println(i + " is Less Than 100 ? "+ lesserthan.test(i));
        }
    }

    @Test
    public void testToIntFunction(){
        List<CityStatePopulation> lstData =
        Stream.of(new CityStatePopulation("New York","New York", 1000000),
                new CityStatePopulation("Boston", "New York", 50000),
                new CityStatePopulation("Miami", "Florida", 1345000),
                new CityStatePopulation("Tampa", "Florida", 120000),
                new CityStatePopulation("Gainesville", "Florida",124921)).
                collect(Collectors.toList());
        ToIntFunction<CityStatePopulation> getPopulation = CityStatePopulation::getPopulation;
        lstData.stream().map(x-> getPopulation.applyAsInt(x)).forEach(System.out::println);
    }

    @Test
    public void testIntFunction(){
        final CityStatePopulation testobj = new CityStatePopulation("Boston", "New York", 0);
        IntFunction<CityStatePopulation> setPopBostonNewYork = i -> {
            testobj.setPopulation(i);
            return testobj;
        };
        System.out.println(setPopBostonNewYork.apply(50000));
    }

    @Test
    public void testFunctionTR(){
        Person aperson = new Person.Builder().withFirstName("Donald").withLastName("Trump").
                withBirthYear(1900).withCountry("USA").build();
        Function<Person, FamilyMember> getFamilyMember = x -> {
          return new FamilyMember(x.getFirstName(), x.getLastName(), 0.0);
        };
        final FamilyMember familyMember =    getFamilyMember.apply(aperson);
        System.out.println(familyMember);

        DoubleFunction<FamilyMember> setIncome = i -> {
            familyMember.setIncome(i);
            return familyMember;
        };
        FamilyMember result = setIncome.apply(4040);
        System.out.println(result);
    }

    @Test
    public void testBitFunction(){
        BiFunction<Integer, Integer, Double> middle = (x,y) -> (double) ((x+y)/2);
        BiFunction<Integer, Integer, Double> sum  = (x,y) -> (double) ((x+y));
        BiFunction<Integer, Integer, Double> dif  = (x,y) -> (double) (Math.abs(x-y));

        List<BiFunction<Integer, Integer, Double>> calcs = new ArrayList<>();

        calcs.add(middle);
        calcs.add(sum);
        calcs.add(dif);

        final int value1 = 100;
        final int value2 = 50;
        calcs.stream().forEach(x -> System.out.println(x.apply(value1, value2)));
    }

    @Test
    public void testUnaryOperator(){
        UnaryOperator<Integer> xor = a -> a ^ 1;
        UnaryOperator<Integer> and = a -> a & 1;
        Function<Integer, Integer> compose = xor.andThen(and);
        System.out.println(compose.apply(4));
    }

    @Test
    public  void testBinaryOperator() {
        Map<String,String> map = new HashMap<>();
        map.put("X", "A");
        map.put("Y", "B");
        map.put("Z", "C");
        BinaryOperator<String> binaryOpt = (s1, s2)-> s1+"-"+s2;
        binaryOperatorFun(binaryOpt, map).forEach(x->System.out.println(x));
    }

    private List<String> binaryOperatorFun(BinaryOperator<String> binaryOpt, Map<String,String> map){
        List<String> biList = new ArrayList<>();
        map.forEach((s1,s2)->biList.add(binaryOpt.apply(s1,s2)));
        return biList;
    }
}

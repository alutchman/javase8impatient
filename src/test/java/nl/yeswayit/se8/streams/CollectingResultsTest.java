package nl.yeswayit.se8.streams;

import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectingResultsTest {

    @Test
    public void collectStringToarray(){
        Stream<String> strStream = Stream.of("One","Two", "Three");

        String[] strArray = strStream.toArray(String[]::new);
        System.out.println(strArray[0]);
    }

    @Test
    public void collectStringToSet(){
        Set<String> strSet = Stream.of("One","Two", "Three").collect(Collectors.toSet());
        HashSet<String> hashSet = Stream.of("One","Two", "Three","One").collect(Collectors.toCollection(HashSet::new));
        TreeSet<String> treeSet = Stream.of("One","Two", "Three","One").collect(Collectors.toCollection(TreeSet::new));
        System.out.println(" Set<String> ="+ strSet);
        System.out.println(" HashSet<String> ="+ hashSet);
        System.out.println(" TreeSet<String> ="+ treeSet);
    }

    @Test
    public void testUsingJoining(){
        String result0 =  Stream.of("One","Two", "Three").
                collect(Collectors.joining());
        String result1 =  Stream.of("One","Two", "Three").
                collect(Collectors.joining("; "));
        System.out.println("result0 = " + result0);
        System.out.println("result1 = " + result1);
    }

    @Test
    public void testIntStatistics(){
        IntSummaryStatistics stats = Stream.of(6,7,8,9,10).
                collect(Collectors.summarizingInt(x -> (int)x));
        System.out.println("stats.getMin() = " + stats.getMin());
        System.out.println("stats.getMax() = " + stats.getMax());
        System.out.println("stats.getAverage() = " + stats.getAverage());
        System.out.println("stats.getCount() = " + stats.getCount());
        System.out.println("stats.getSum() = " + stats.getSum());
    }

    @Test
    public void testDoubleStatistics(){
        DoubleSummaryStatistics stats = Stream.of(6.6, 7.5, 8.4, 9.2, 10.7).
                collect(Collectors.summarizingDouble(x -> (double)x));
        System.out.println("stats.getMin() = " + stats.getMin());
        System.out.println("stats.getMax() = " + stats.getMax());
        System.out.println("stats.getAverage() = " + stats.getAverage());
        System.out.println("stats.getCount() = " + stats.getCount());
        System.out.println("stats.getSum() = " + stats.getSum());
    }
}

package nl.yeswayit.se8.streams;

import org.junit.Test;

import java.util.DoubleSummaryStatistics;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class PrimitiveTypeStreamTest {

    @Test
    public void defineExampleTest(){
        Stream<String> words = Stream.of("Dit", "zijn", "meerdere", "woorden");
        IntStream lengths = words.mapToInt(String::length);
        List<Integer> lstLength =lengths.boxed().collect(Collectors.toList());
        System.out.println(lstLength);

        IntStream unboxed = IntStream.range(0,6);
        int[] intArray = unboxed.toArray();

        Stream<Integer> boxedInt = IntStream.range(0,7).boxed();
        IntStream intArray2 =boxedInt.mapToInt(i -> (int) i);

       Object[] intObjArray =Stream.of(12,13,14,15).toArray();
       int[] intArrayx =Stream.of(12,13,14,15).mapToInt(i -> (int) i).toArray();

        Stream<Character> charstr = Stream.of('A','B', '1','2', '@');
        IntStream fromChar = charstr.mapToInt(i -> (int) i);
        List<Character> charList =fromChar.mapToObj(i -> (char)i).collect(Collectors.toList());
        System.out.println(charList);
    }

    @Test
    public void testOptionalDoubleInt(){
        DoubleStream doubleStream = DoubleStream.of(10.50, 23.20, 15.9, 53.11);
        OptionalDouble optionalDouble =doubleStream.min();
        System.out.println(optionalDouble.getAsDouble());

        IntStream intstr = IntStream.of(10, 20, 30, 40);
        OptionalInt optionalInt = intstr.max();
        System.out.println("Valid Intstr" + optionalInt.orElse(0));

        optionalInt = OptionalInt.empty();
        System.out.println("For Empty: "+optionalInt.orElse(0));
    }

    @Test
    public void testSummaryStatistics(){
        DoubleStream doubleStream = DoubleStream.of(10.50, 23.20, 15.9, 53.11);
        DoubleSummaryStatistics doubleSummaryStatistics = doubleStream.summaryStatistics();
        System.out.println(doubleSummaryStatistics);
        //Result=DoubleSummaryStatistics{count=4, sum=102.710000, min=10.500000, average=25.677500, max=53.110000}

        LongStream longStream = LongStream.of(10l, 200l, 1500l, 50l);
        LongSummaryStatistics longSummaryStatistics = longStream.summaryStatistics();
        System.out.println(longSummaryStatistics);
        //result=LongSummaryStatistics{count=4, sum=1760, min=10, average=440.000000, max=1500}

        IntStream intStream = IntStream.range(100, 105);
        IntSummaryStatistics intSummaryStatistics = intStream.summaryStatistics();
        System.out.println(intSummaryStatistics);
        //result=IntSummaryStatistics{count=5, sum=510, min=100, average=102.000000, max=104}
    }

}

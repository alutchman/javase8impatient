package nl.yeswayit.se8;

import nl.yeswayit.se8.utils.Person;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFileAttributes;
import java.nio.file.attribute.PosixFilePermissions;
import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class ErrorsCheck {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void test001(){
        List<StringBuilder> messages = Arrays.asList(new StringBuilder(), new StringBuilder());
        messages.stream().forEach(s->s.append("helloworld"));
        messages.forEach(s->{
            s.insert(5,",");
            System.out.println(s);
        });
    }

    @Test
    public void test002(){
        List<?> messages = new ArrayList<Integer>();
        //  messages.add("");  --> compile error
        List<String>  strList = Arrays.asList("een", "twee", "drie");
        List<Integer> intlist = Arrays.asList(1,2,3,4);
        printlist(strList);
        printlist(intlist);
    }

    public void printlist(List<?> aList) {
        System.out.println(aList);
    }

    @Test
    public void test003() throws IOException {
        Path path1 = Paths.get(".\\Test");
        System.out.println(path1.getRoot());
        System.out.println(path1.toUri());
        System.out.println(path1.toAbsolutePath());

        Files.deleteIfExists(path1);
        Files.createDirectory(path1);
        System.out.println(path1.toRealPath(LinkOption.NOFOLLOW_LINKS));

        Files.deleteIfExists(path1);
    }

    @Test
    public void test004() throws IOException {
        expectedException.expect(NoSuchFileException.class);
        expectedException.expectMessage("/Users/amritlutchman/IdeaProjects/CertifyJse8/Test");
        Path path1 = Paths.get(".//Test");
        System.out.println(path1.toRealPath(LinkOption.NOFOLLOW_LINKS));
    }

    @Test
    public void test005() throws IOException {
        Path path1 = Paths.get(".//Test");
        Files.deleteIfExists(path1);
        Files.createDirectory(path1);
        System.out.println(path1.toRealPath(LinkOption.NOFOLLOW_LINKS));
        Files.delete(path1);
    }

    @Test
    public void test006() throws IOException {
        Path path1 = Paths.get("src");
        Path path2 = Paths.get("main/resources");
        System.out.println(path1.toRealPath(LinkOption.NOFOLLOW_LINKS));
        System.out.println(path1.resolve(path2).toString());

    }

    @Test
    public void test007() throws IOException {
        Path path1 = Paths.get("src/main/resources/../java");

        System.out.println(path1.normalize().toString());
    }

    @Test
    public void test008(){
        assertNull(Paths.get(".//test").getRoot());
        assertNotNull(Paths.get("/D://OCPJP//tttt").getRoot());
    }

    @Test
    public void test009() throws IOException {
        Path file = Paths.get("./target");
        PosixFileAttributes attr =
                Files.readAttributes(file, PosixFileAttributes.class);
        System.out.format("%s %s %s%n",
                attr.owner().getName(),
                attr.group().getName(),
                PosixFilePermissions.toString(attr.permissions()));
    }

    @Test
    public void test010()  {
        LocalDate nlnu = LocalDate.now(Clock.systemUTC());

        System.out.println(LocalDate.now(Clock.systemUTC()));
        System.out.println(LocalDate.now(Clock.system(ZoneId.of("Pacific/Auckland"))));
        System.out.println(LocalDate.now(ZoneId.of("Pacific/Auckland")));

        Period p1 = Period.parse("P2Y24M40D");

        System.out.println( nlnu.plus(p1));

        System.out.println(LocalTime.ofSecondOfDay(86399));
        System.out.println(LocalTime.now());
        System.out.println(LocalDateTime.now());
        ZoneId.getAvailableZoneIds().stream().filter(x -> x.contains("Europe/A")).forEach(System.out::println);

        int value = 1000000;

        try(InputStream is = new FileInputStream("")){

        } catch (IOException | IndexOutOfBoundsException e) {
            e.printStackTrace();
            //e = new FileNotFoundException();
        }
    }

    @Test
     public void test0001() {
        expectedException.expect(NullPointerException.class);
        String[] sa = new String[]{ "a", "b", null};
        for(String s :  sa){
            switch(s){
                case "a" : System.out.println("Got a");
            }
        }

    }


}

package nl.yeswayit.se8.concurrency;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableAndServiceDemo {
    public static void main(String[] args)  {
        ExecutorService es = Executors.newSingleThreadExecutor();
        List<Factorial> taskList = new ArrayList<>();
        for(long n = 5; n <10; n++){
            //Callable<Long> task = new Factorial(n);
            taskList.add(new Factorial(n));
        }
        List<Future<Long>> futureList = new ArrayList<>();
        //Future<Long> future = es.submit(task1);
        taskList.stream().map(task -> es.submit(task)).forEach(futureList::add);

        try {
            for(int i = 0; i < taskList.size();  i++){
                Factorial task = taskList.get(i);
                Future<Long> future = futureList.get(i);
                System.out.printf("Factorial of %d is %d \n", task.getN(), future.get());
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }  finally{
            es.shutdown();
        }
    }

    private static class Factorial implements Callable<Long> {
        long n;

        public Factorial(long n) {
            this.n = n;
        }

        @Override
        public Long call() throws Exception {
            if (n < 0) {
                throw new IOException("Number has to be greater dan 0");
            }

            long factor=1;
            for(long index=1; index<=n; index++){
                factor *= index;
            }
            return factor;
        }

        public long getN() {
            return n;
        }
    }
}

package nl.yeswayit.se8.concurrency;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.junit.Assert.assertEquals;

public class CopyOnWriteArrayListDemo {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void erroOnWrite(){
        expectedException.expect(ConcurrentModificationException.class);
        List<String> aList = new ArrayList<>();
        aList.add("one");
        aList.add("two");
        aList.add("three");

        Iterator iterator = aList.iterator();
        while(iterator.hasNext()) {
            System.out.println(iterator.next());
            aList.add("four");
        }
    }

    @Test
    public void happyFlow(){
        List<String> aList = new CopyOnWriteArrayList<>();
        aList.add("one");
        aList.add("two");
        aList.add("three");

        Iterator iterator = aList.iterator();
        while(iterator.hasNext()) {
            System.out.println(iterator.next());
            aList.add("four");
        }
        assertEquals(6, aList.size());
    }
}

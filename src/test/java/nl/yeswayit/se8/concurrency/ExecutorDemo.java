package nl.yeswayit.se8.concurrency;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

public class ExecutorDemo {
    public static void main(String[] args) {
        Runnable runnable = new Task();
        System.out.println("Calling Task.run() by dictly calling a thread");
        Thread thread = new Thread(runnable);
        thread.setName("Direct Thread");
        thread.setPriority(1);
        thread.start();

        RepeatedExecutor repeatedExecutor = new RepeatedExecutor();
        repeatedExecutor.execute(runnable, 3);
    }
}

class Task implements Runnable {
    @Override
    public void run() {
        System.out.println("Calling Task.run() "+Thread.currentThread().getName());
    }
}

class RepeatedExecutor implements Executor {
    private final AtomicInteger counter = new AtomicInteger(0);
    @Override
    public void execute(@NotNull Runnable command) {
        new Thread(command, "Executor nr. "+counter.incrementAndGet()).start();
    }

    public void execute(Runnable runnable, int times) {
        System.out.printf("Calling Task.run() %d times through executor.execute() \n ", times);
        for (int i = 0; i <3; i++) {
            execute(runnable);
        }
    }
}
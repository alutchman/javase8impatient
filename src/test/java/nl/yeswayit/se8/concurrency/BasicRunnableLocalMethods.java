package nl.yeswayit.se8.concurrency;

import java.util.concurrent.atomic.AtomicInteger;

public class BasicRunnableLocalMethods {
    private AtomicInteger counter = new AtomicInteger(0);

    public void run(){
        increment();
        increment();
        increment();
    }

    public void increment(){
        int current = counter.incrementAndGet();
        System.out.print( current +"  ");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args){
        BasicRunnableLocalMethods useCounter = new BasicRunnableLocalMethods();
        Thread t1 = new Thread(useCounter::run);
        Thread t2 = new Thread(useCounter::run);
        Thread t3 = new Thread(useCounter::run);

        t1.setPriority(1);
        t2.setPriority(1);
        t3.setPriority(1);

        t1.start();
        t2.start();
        t3.start();
    }


}

package nl.yeswayit.se8.concurrency;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.stream.LongStream;

public class SumOfN_UsingFokJoin {
    private static long N = 1000_000; //One Million.. we want to compute the sum
    private static final int NUM_THREADS = 10;

    static class RecursiveSumOfN extends RecursiveTask<Long> {
        long from, to;

        public RecursiveSumOfN(long from, long to) {
            if (to < from) {
                this.from = to;
                this.to = from;
            } else {
                this.from = from;
                this.to = to;
            }
        }

        @Override
        protected Long compute() {
            if (to - from <= NUM_THREADS) {
                return this.handleDirectly();
            } else {
                return handleByForking();
            }
        }

        private long handleByForking() {
            long mid = (from + to)/2;
            System.out.printf("Forking computation into 2 ranges: " +
                    "%d to %d and %d to %d \n", from, mid, mid+1, to);
            RecursiveSumOfN firstHalf = new RecursiveSumOfN(from,mid);

            //Fork off the task
            firstHalf.fork();

            RecursiveSumOfN secondHalf = new RecursiveSumOfN(mid +1,to);
            long resultSecond = secondHalf.compute();
            return firstHalf.join() + resultSecond;
        }

        private Long handleDirectly() {
            LongStream fromTos = LongStream.range(from,to+1);
            long computedSum = fromTos.sum();
            System.out.printf("\tSum of value in range %d to %d  is %d %n ", from, to, computedSum);
            return computedSum;
        }
    }

    public static void main(String[] args){
        long from = 1;
        long to = N;
        ForkJoinPool pool = new ForkJoinPool(NUM_THREADS);
        long computedSum = pool.invoke(new RecursiveSumOfN(from, to));

        long formulasum = (to*(to+1))/2;
        System.out.printf("Sum of value in range %d..%d  computed sum = %d %n \t" +
                "formula sum = %d ", from, to, computedSum,formulasum);
        System.out.println("Parallelism Pool : "+ForkJoinPool.commonPool().getParallelism());
    }
}

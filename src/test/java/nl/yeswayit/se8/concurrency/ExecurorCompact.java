package nl.yeswayit.se8.concurrency;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.atomic.AtomicInteger;

public class ExecurorCompact {
    private final AtomicInteger counter = new AtomicInteger(0);

    public void execute(@NotNull Runnable command) {
        new Thread(command, "Executor nr. "+counter.incrementAndGet()).start();
    }

    public void run() {
        System.out.println("Calling Task.run() "+Thread.currentThread().getName());
    }

    public void execute(Runnable runnable, int times) {
        System.out.printf("Calling Task.run() %d times through executor.execute() \n ", times);
        for (int i = 0; i <3; i++) {
            execute(this::run);
        }
    }

    public static void main(String[] args) {
        ExecurorCompact executorCompact = new ExecurorCompact();
        System.out.println("Calling Task.run() by dictly calling a thread");
        Thread thread = new Thread(executorCompact::run);
        thread.setName("Direct Thread");
        thread.setPriority(1);
        thread.start();
        executorCompact.execute(executorCompact::run, 3);
    }
}

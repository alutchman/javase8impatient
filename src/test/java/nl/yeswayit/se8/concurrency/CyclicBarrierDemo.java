package nl.yeswayit.se8.concurrency;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierDemo {
    private CyclicBarrier waitPoint;

    public CyclicBarrierDemo(int totalPlayers){
        waitPoint = new CyclicBarrier(totalPlayers, this::handleMixedDoubleTennisGame);
    }

    //Callback when all required plays have registerd
    public void handleMixedDoubleTennisGame(){
        System.out.println("\nAll players are ready. \nGame will start now!");
    }

    public void registerNext( String name){
        new Thread(this::playerReady, name).start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void playerReady() {
        System.out.println("Player "+ Thread.currentThread().getName() + " is ready");
        try {
            waitPoint.await();
        } catch (InterruptedException | BrokenBarrierException e) {
            System.out.println("Exception.occured : "+ e);
        }
    }

    public static void main(String[] args) {
        System.out.println("Reserving Tennis court.");
        System.out.println("As soon as the 4 players arrive the match will begin.\n");
        CyclicBarrierDemo cyclicBarrierDemo = new CyclicBarrierDemo(4);
        cyclicBarrierDemo.registerNext("GI Joe");
        cyclicBarrierDemo.registerNext( "Super Manne");
        cyclicBarrierDemo.registerNext( "Boy George");
        cyclicBarrierDemo.registerNext( "Nathan Cool");
    }
}

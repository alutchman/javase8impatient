package nl.yeswayit.se8.concurrency;

import org.junit.Test;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.LongStream;

public class ParallelStreamsDemo {
    private static boolean isPrime(long val){
        for (long i=2; i <=val/2; i++) {
            if (val %i ==0) {
                return false;
            }
        }
        return true;
    }

    @Test
    public void NonParallelExample(){
        Instant startTime = Instant.now();
        long numPrimes = LongStream.rangeClosed(1, 10_000).
                filter(ParallelStreamsDemo::isPrime).count();
        System.out.println(numPrimes);

        Instant endTime = Instant.now();

        Duration dur = Duration.between(startTime, endTime);
        System.out.printf("Duration %d ms.\n", dur.toMillis());
    }

    @Test
    public void aParallelExample(){
        Instant startTime = Instant.now();
        long numPrimes = LongStream.rangeClosed(1, 10_000).parallel().
                filter(ParallelStreamsDemo::isPrime).count();
        System.out.println(numPrimes);

        Instant endTime = Instant.now();

        Duration dur = Duration.between(startTime, endTime);
        System.out.printf("Duration %d ms.\n", dur.toMillis());
    }

    @Test
    public void anotherParallelExample(){
        Instant startTime = Instant.now();

        List<Long> longList = new CopyOnWriteArrayList<>();
        LongStream.rangeClosed(1, 10_000).
                parallel().filter(ParallelStreamsDemo::isPrime).forEach(longList::add);

        long numPrimes = longList.size();
        System.out.println(numPrimes);

        Instant endTime = Instant.now();

        Duration dur = Duration.between(startTime, endTime);
        System.out.printf("Duration %d ms.\n", dur.toMillis());
    }


}

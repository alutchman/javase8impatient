package nl.yeswayit.se8.localize;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Arrays;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import static org.junit.Assert.assertEquals;

public class ResourceDemo {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void printLocale(){
        Locale currentLocaleNL = Arrays.stream(Locale.getAvailableLocales()).filter(this::isNetherlands).findFirst().orElse(Locale.getDefault());
        showChountryInfo(currentLocaleNL);

        Locale currentLocale =  new Locale("nl","NL");
        Locale.setDefault(currentLocale);
        showChountryInfo(Locale.getDefault());
        Locale.setDefault(new Locale("en","NL"));
    }

    private boolean isNetherlands(Locale item){
        return item.getCountry().equals("NL")
                && item.getDisplayCountry().equals("Netherlands") &&
                item.getLanguage().equals("nl");
    }

    private void showChountryInfo(Locale otherLocale){
        System.out.printf("%s - %s %s\t",otherLocale.getCountry(),otherLocale.getLanguage(), otherLocale.getDisplayCountry());
        System.out.println(otherLocale.toString());
    }

    @Test
    public void testProperties(){
        //When not setting a default .. failure to find a locale
        // will result in a MissingResourceException
        Locale.setDefault(new Locale("nl","NL"));

        ResourceBundle mybundle = ResourceBundle.getBundle("MyLabels");
        assertEquals("Verjaardag",mybundle.getString("label1"));

        mybundle = ResourceBundle.getBundle("MyLabels",Locale.US);
        assertEquals("BirthDay", mybundle.getString("label1"));

        mybundle = ResourceBundle.getBundle("MyLabels",Locale.ITALY);
        assertEquals("Verjaardag", mybundle.getString("label1"));

        Locale.setDefault(new Locale("en","NL"));
      }

      @Test
      public void findCandidateLocales(){
          expectedException.expect(MissingResourceException.class);
          expectedException.expectMessage("Can't find resource for bundle java.util.PropertyResourceBundle, key label2");
          Locale.setDefault(new Locale("nl","NL"));

          ResourceBundle mybundle = ResourceBundle.getBundle("MyLabels");
          mybundle.getString("label2");
      }

    @Test
    public void findCandidateLocalesNotDefined(){
        expectedException.expect(MissingResourceException.class);
        expectedException.expectMessage("Can't find bundle for base name MyLabels, locale nl_BE");

        ResourceBundle mybundle = ResourceBundle.getBundle("MyLabels",
                new Locale("nl","BE"));
        mybundle.getString("label1");
    }
}

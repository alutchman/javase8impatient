package nl.yeswayit.se8.javaio;

import java.io.IOException;

public class ConsoleTest {

    public static void main(String[] args){
        System.out.println("Console example");
        int intval = 0;

        try {
            System.out.print("Type an integer: ");
            intval = System.in.read();
        } catch(IOException e) {
            System.err.println("Cannot read input:"+ e);
            System.exit(-1);
        }
        System.out.println("You typed "+ (char) intval);
    }
}

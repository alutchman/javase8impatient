package nl.yeswayit.se8.javaio;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Base64;
import java.util.Map;
import java.util.function.BiPredicate;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class NioPaths {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void showPathInfo() {
        Path target  = Paths.get(System.getProperty("user.dir") + "\\target");
        assertEquals(4, target.getNameCount());
        assertEquals("C:\\Java\\projects\\javase8impatient\\target", target.toAbsolutePath().toString());
        assertEquals("Java", target.getName(0).toString());
        assertEquals("projects", target.getName(1).toString());
        assertEquals("javase8impatient", target.getName(2).toString());
        assertEquals("target", target.getName(3).toString());
    }

    @Test
    public void readTextFileWithFiles() throws URISyntaxException {
        ClassLoader classLoader = getClass().getClassLoader();
        Path filePath = Paths.get(classLoader.getResource("appoint01.txt").toURI());
        try(Stream<String> data = Files.lines(filePath).
                filter(x -> x.trim().length() > 0).map(String::toLowerCase)) {
            data.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void readFromUrl() {
        try {
            URL url = new URL("https://www.yeswayit.nl/");
            try(BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()))) {
                Stream<String> lines = reader.lines();
                lines.filter(x -> x.trim().length() > 0).forEach(System.out::println);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void showAllPaths(){
        Path target  = Paths.get(System.getProperty("user.dir"));
        try(Stream<Path> dirInfo = Files.list(target)){
            dirInfo.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void showAllPathsJava7(){
        Path target  = Paths.get(System.getProperty("user.dir"));
        try(DirectoryStream<Path> dirInfo = Files.newDirectoryStream(target)){
            dirInfo.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void showAllPathsWithSubs(){
        Path target  = Paths.get(System.getProperty("user.dir"));
        try(Stream<Path> dirInfo = Files.walk(target)){
            dirInfo.
                    filter(x -> x.toFile().isDirectory()
                            && !x.toFile().isHidden()
                            && !x.toString().contains(".git")
                            && !x.toString().contains(".idea")
                            && !x.toString().contains("target")).
                    forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void showBase64EncodeDecode(){
        Base64.Encoder encoder = Base64.getEncoder();
        Base64.Decoder decoder = Base64.getDecoder();
        String example = "Username: geekOrDummy From Mars";
        String encoded = encoder.encodeToString(example.getBytes(StandardCharsets.UTF_8));
        String decoded = new String(decoder.decode(encoded.getBytes(StandardCharsets.UTF_8)));
        System.out.println("starting with:"+ example);
        System.out.println("encoded :"+ encoded);
        System.out.println("decoded :"+ decoded);
    }

    @Test
    public void tempfiles(){
        Path target  = Paths.get(System.getProperty("user.dir")+"\\target");
        try {
            Path newPath = Files.createTempFile(target, "NL", ".txt");
            System.out.println(newPath);
            newPath = Files.createTempFile("NL", ".txt");
            System.out.println(newPath);
            newPath = Files.createTempDirectory("NL");
            System.out.println(newPath);
            newPath = Files.createTempDirectory(target,"NL");
            System.out.println(newPath);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void showAllAttribTarget(){
        Path target  = Paths.get(System.getProperty("user.dir")+"\\target");
        try {
            Map<String ,Object> results = Files.readAttributes(target, "*");
            System.out.println(results);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void showBasicAttribTarget(){
        Path target  = Paths.get(System.getProperty("user.dir")+"\\target");
        try {
            BasicFileAttributes results = Files.readAttributes(target, BasicFileAttributes.class);
            printBasicAttributes(results);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void printBasicAttributes(BasicFileAttributes attributes)  {
        System.out.println("-- Some BasicFileAttributes --");
        System.out.printf("creationTime     = %s%n", attributes.creationTime());
        System.out.printf("lastAccessTime   = %s%n", attributes.lastAccessTime());
        System.out.printf("lastModifiedTime = %s%n", attributes.lastModifiedTime());
        System.out.printf("size             = %s%n", attributes.size());
        System.out.printf("directory        = %s%n", attributes.isDirectory());
    }

    @Test
    public void findingFiles(){
        Path target  = Paths.get(System.getProperty("user.dir")+
                "\\target\\test-classes\\nl\\yeswayit\\se8");
        BiPredicate<Path,BasicFileAttributes> predicate = (path, attrs)  ->
                attrs.isRegularFile() && path.toString().endsWith(".class");
        /*
        2 is the current depth to search for. If we use 1 in our example , we get no files
        since we would then be looking for files in only the se8 directory and not deeper
         */
        try(Stream<Path> entries = Files.find(target, 2, predicate )){
            entries.limit(10).forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void findingFilesFromResource() throws URISyntaxException {
        ClassLoader classLoader = getClass().getClassLoader();
        Path target  = Paths.get(classLoader.getResource(".").toURI());
        BiPredicate<Path,BasicFileAttributes> predicate = (path, attrs)  ->
                attrs.isRegularFile() && !path.toString().contains("\\~") && path.toString().endsWith(".docx");
        try(Stream<Path> entries = Files.find(target, 100, predicate )){
            entries.limit(10).forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void normalizeExampe(){
        Path path = Paths.get("C:\\..\\..\\.\\projects\\javase8impatient"
                + "\\target\\test-classes");
        // print actual path
        System.out.println("Actual Path : "   + path);
        // normalize the path
        Path normalizedPath = path.normalize();
        // print normalized path
        System.out.println("Normalized Path : " + normalizedPath);
        assertEquals("target", normalizedPath.subpath(2,3).toString());
        //from 2 till endindex 3 non inclusive
        System.out.println("Normalized sub Path(2,3) : " + normalizedPath.subpath(2,3));
    }

    @Test
    public void createMultiDirecoriesAtOnce() throws IOException {
        Path target  = Paths.get(System.getProperty("user.dir")+"\\target\\level1\\level2\\level3");
        Files.createDirectories(target);
    }

    @Test
    public void createMultiDirecorieError() throws IOException {
        expectedException.expect(NoSuchFileException.class);
        expectedException.expectMessage("C:\\Java\\projects\\javase8impatient\\target\\level1b\\level2b\\level3b");
        Path target  = Paths.get(System.getProperty("user.dir")+"\\target\\level1b\\level2b\\level3b");
        Files.createDirectory(target);
    }
}

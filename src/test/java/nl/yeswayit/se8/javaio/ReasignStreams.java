package nl.yeswayit.se8.javaio;

import java.io.IOException;
import java.io.PrintStream;
import java.time.LocalDate;

public class ReasignStreams {
    public static void main(String[] args){
        System.out.println("Console example");
        int intval = 0;

        try {
            System.setOut(new PrintStream("c:/Users/arwlu/Downloads/data.txt"));
            System.setErr(new PrintStream("c:/Users/arwlu/Downloads/error.txt"));
            System.out.println("Regel op "+ LocalDate.now());
            throw  new IOException("Boring test error...");
        } catch(IOException e) {
            System.err.println("Cannot read input:"+ e);
            System.exit(-1);
        }

    }
}

package nl.yeswayit.se8.javaio;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.io.Serializable;

@Builder
@EqualsAndHashCode
@Getter
public class Student implements Serializable {
    private static final long serialVersionUID = 8370732650730621298L;

    public enum Level {
        SOPHMORE, JUNIOR, SENIOR, BACHELOR, MASTER
    }
    private String firstName;
    private String lastName;
    private Integer studentNumber;
    private Integer startYear;
    private String major;
    private Level currentLevel;
    private transient Double gpa;


    public void setGpa(Double gpa) {
        this.gpa = gpa;
    }

}

package nl.yeswayit.se8.javaio;

import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class WriteObjectSreams {

    @Test
    public void demoObjectStreams(){
        String pathTemp=System.getProperty("user.dir")+"\\target\\";
        File destFile =  new File(pathTemp+"student.bin");

        Student student = Student.builder().currentLevel(Student.Level.BACHELOR)
                .firstName("Coco").lastName("Cabana").major("Electrical Enginering")
                .startYear(2018).studentNumber(999944851).gpa(7.583).build();
        try(ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(destFile))) {
            os.writeObject(student);
            if (student.getGpa() != null) {
                os.writeDouble(student.getGpa());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Student studentRead = null;
        try(ObjectInputStream is = new ObjectInputStream(new FileInputStream(destFile))) {
            Double gpa = null;
            Object result = is.readObject();
            if (result != null && result instanceof Student) {
                studentRead = (Student)result;
                studentRead.setGpa(gpa);
            }
            if (is.available() > 0) {  //is there more to read
                gpa = is.readDouble();
                studentRead.setGpa(gpa);
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        assertNotNull(studentRead);
        assertEquals(student, studentRead);
        assertEquals(student.getGpa(), studentRead.getGpa());
    }
}

package nl.yeswayit.se8.javaio;

import org.junit.Test;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ReadWriteByteFiles {
    private final ClassLoader classLoader = getClass().getClassLoader();
    @Test
    public void readTux(){
        File srcFile =  new File(classLoader.getResource("tux.png").getFile());
        Long totalBytes = srcFile.length();
        System.out.println("Size in bytes:"+ totalBytes);
        byte[] result = null;
        try(FileInputStream fis = new FileInputStream(srcFile);
            BufferedInputStream bis = new BufferedInputStream(fis);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();) {

            byte[] buffer = new byte[totalBytes.intValue()];
            while (bis.available() > 0) {
                bis.read(buffer);
                outputStream.write(buffer);
            }
            result =  outputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            assertNotNull(result);
            assertEquals(totalBytes.intValue(), result.length);
        }
    }

    @Test
    public void writeAndReadBack(){
        String pathTemp=System.getProperty("user.dir")+"\\target\\";
        File destFile =  new File(pathTemp+"data.bin");
        try(DataOutputStream fos = new DataOutputStream(new FileOutputStream(destFile)))   {
            for (int i =0; i < 10; i++){
                fos.writeByte(i);
                fos.writeShort(i);
                fos.writeInt(i);
                fos.writeLong(i);
                fos.writeFloat(i*1.005f);
                fos.writeDouble(i * 1.081001);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try(DataInputStream fis = new DataInputStream(new FileInputStream(destFile)))   {
            for (int i =0; i < 10; i++){
                System.out.printf("%d\t%d\t%d\t%d\t%g\t%g \n",
                        fis.readByte(), fis.readShort(),fis.readInt(),
                        fis.readLong(), fis.readFloat(), fis.readDouble());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

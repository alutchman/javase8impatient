package nl.yeswayit.se8.javaio;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReadWriteTextFiles {
    private final ClassLoader classLoader = getClass().getClassLoader();
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testTryWithResources(){
        File aFile = new File(classLoader.getResource("J8Upgrade.txt").getFile());
        try (Stream<String> lines = Files.lines(aFile.toPath())) {
            lines.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void readDirectly(){
        //it will give the root directory of where your program runs
        File srcFile =  new File(classLoader.getResource("J8Upgrade.txt").getFile());
        try(FileReader input = new FileReader(srcFile)) {
            int ch;
            while ( (ch = input.read()) != -1) {
                System.out.print((char) ch);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void writeInTarget(){
        String pathTemp=System.getProperty("user.dir")+"\\target\\";
        //it will give the root directory of where your program runs
        File srcFile =  new File(classLoader.getResource("J8Upgrade.txt").getFile());
        File destFile =  new File(pathTemp+"copied.txt");
        try(
                BufferedReader input = new BufferedReader(new FileReader(srcFile));
                BufferedWriter output = new BufferedWriter(new FileWriter(destFile))
            ) {
            int ch = 0;
            while ( (ch = input.read()) != -1) {
                output.write((char) ch);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void tokenizeData(){
        File srcFile =  new File(classLoader.getResource("token.txt").getFile());
        Set<String> words = new TreeSet<>();
        try (Scanner tokenScanner = new Scanner(new FileReader(srcFile))){
            tokenScanner.useDelimiter("\\W");
            while(tokenScanner.hasNext()) {
                String word = tokenScanner.next().replaceAll("\\.","").trim();
                if (word.length() > 0) {
                    words.add(word.toLowerCase());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String result =  words.stream().collect(Collectors.joining("; "));
        System.out.println(result);
    }
}
